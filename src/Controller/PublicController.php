<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class PublicController extends AbstractController
{
	private $translator;

	public function __construct(TranslatorInterface $translator)
	{
		$this->translator = $translator;
	}

	/**
	 * @Route("/login", name="login")
	 */
	public function login(AuthenticationUtils $authenticationUtils): Response
	{
		if ($this->getUser()) {
			return $this->redirectToRoute('public_home');
		}

		$session = $this->container->get('request_stack')->getSession();
		$error = $authenticationUtils->getLastAuthenticationError();
		$lastUsername = $authenticationUtils->getLastUsername();

		return $this->render('@EasyAdmin/page/login.html.twig', [
			'error' => $error,
			'last_username' => $lastUsername,
			'target_path' => $this->generateUrl('public_home'),
			'page_title' => $this->translator->trans('public.login.title'),
			'username_label' => $this->translator->trans($session->get('config')->enableUsername ? 'entities.user.fields.username' : 'entities.user.fields.email'),
			'password_label' => $this->translator->trans('entities.user.fields.password'),
			'forgot_password_label' => $this->translator->trans('public.login.forgotPassword'),
			'remember_me_label' => $this->translator->trans('public.login.rememberMe'),
			'sign_in_label' => $this->translator->trans('public.login.loginBtn'),
			'remember_me_enabled' => true,
			'remember_me_checked' => true,
			'app_logo' => $session->get('config')->appLogo,
		]);
	}

	/**
	 * @Route("/logout", name="logout")
	 */
	public function logout(): void
	{
	}

	/**
	 * Returns the corresponding redirect, if the user cannot access.
	 */
	public function checkPublicAccess()
	{
		$session = $this->container->get('request_stack')->getSession();

		if ($this->isGranted('ROLE_ADMIN')) {
			return $this->redirectToRoute('admin');
		} else if (!$session->get('config')->enablePublic) {
			return $this->redirectToRoute($this->getUser() ? 'admin' : 'login');
		}

		return null;
	}

	/**
	 * @Route("/", name="public_home")
	 */
	public function public_home(Request $request): Response
	{
		$redirect = $this->checkPublicAccess();
		if ($redirect) {
			return $redirect;
		}

		return $this->render('public/home.html.twig');
	}
}
