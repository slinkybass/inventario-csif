<?php

namespace App\Controller\Admin\Filters;

use App\Entity\Departamento;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Filter\FilterInterface;
use EasyCorp\Bundle\EasyAdminBundle\Filter\FilterTrait;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FilterDataDto;
use EasyCorp\Bundle\EasyAdminBundle\Form\Filter\Type\EntityFilterType;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\ComparisonType;

class DepartamentoFilter implements FilterInterface
{
    use FilterTrait;

    public static function new(string $propertyName, $label = null): self
    {
        return (new self())
            ->setFilterFqcn(__CLASS__)
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setFormType(EntityFilterType::class)
			->setFormTypeOption('value_type_options', [
                'class' => Departamento::class
            ]);
    }

    public function apply(QueryBuilder $queryBuilder, FilterDataDto $filterDataDto, ?FieldDto $fieldDto, EntityDto $entityDto): void
    {
        $comparison = $filterDataDto->getComparison();
        $value = $filterDataDto->getValue();

        $queryBuilder->leftJoin('entity.puestos', 'puesto');
        $queryBuilder->leftJoin('puesto.despacho', 'despacho');
        $queryBuilder->leftJoin('despacho.departamento', 'departamento');

		if (ComparisonType::CONTAINS == $comparison) {
			$words = explode(' ', trim($value, '%'));
			foreach ($words as $k => $word) {
				$queryBuilder->andWhere("departamento " . $comparison . " :val_departamento_" . $k)->setParameter('val_departamento_' . $k, '%' . $word . '%');
			}
		} else {
			$queryBuilder->andWhere("departamento " . $comparison . " :val_departamento")->setParameter('val_departamento', $value);
		}
    }
}