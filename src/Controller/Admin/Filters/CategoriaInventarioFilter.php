<?php

namespace App\Controller\Admin\Filters;

use App\Entity\CategoriaInventario;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Filter\FilterInterface;
use EasyCorp\Bundle\EasyAdminBundle\Filter\FilterTrait;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FilterDataDto;
use EasyCorp\Bundle\EasyAdminBundle\Form\Filter\Type\EntityFilterType;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\ComparisonType;

class CategoriaInventarioFilter implements FilterInterface
{
    use FilterTrait;

    public static function new(string $propertyName, $label = null): self
    {
        return (new self())
            ->setFilterFqcn(__CLASS__)
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setFormType(EntityFilterType::class)
			->setFormTypeOption('value_type_options', [
                'class' => CategoriaInventario::class
            ]);
    }

    public function apply(QueryBuilder $queryBuilder, FilterDataDto $filterDataDto, ?FieldDto $fieldDto, EntityDto $entityDto): void
    {
        $comparison = $filterDataDto->getComparison();
        $value = $filterDataDto->getValue();

        $queryBuilder->leftJoin('entity.tipoInventario', 'tipoInventario');
        $queryBuilder->leftJoin('tipoInventario.categoriaInventario', 'categoriaInventario');

		if (ComparisonType::CONTAINS == $comparison) {
			$words = explode(' ', trim($value, '%'));
			foreach ($words as $k => $word) {
				$queryBuilder->andWhere("categoriaInventario " . $comparison . " :val_categoriaInventario_" . $k)->setParameter('val_categoriaInventario_' . $k, '%' . $word . '%');
			}
		} else {
			$queryBuilder->andWhere("categoriaInventario " . $comparison . " :val_categoriaInventario")->setParameter('val_categoriaInventario', $value);
		}
    }
}