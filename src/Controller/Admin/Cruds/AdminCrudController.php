<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\User;
use App\Entity\Role;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class AdminCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;
	private $passwordHasher;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService, UserPasswordHasherInterface $passwordHasher)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
		$this->passwordHasher = $passwordHasher;
	}

	public static function getEntityFqcn(): string
	{
		return User::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$session = $this->container->get('request_stack')->getSession();

		if ($session->get('config')->enablePublic) {
			$crud->setEntityLabelInSingular($this->translator->trans('entities.admin.singular'));
			$crud->setEntityLabelInPlural($this->translator->trans('entities.admin.plural'));
		} else {
			$crud->setEntityLabelInSingular($this->translator->trans('entities.user.singular'));
			$crud->setEntityLabelInPlural($this->translator->trans('entities.user.plural'));
		}
		$crud->setDefaultSort(['email' => 'ASC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			if ($session->get('config')->enablePublic) {
				$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.admin.singular') . ': ' . $entity);
				$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
					'%entity_label_singular%' => $this->translator->trans('entities.admin.singular') . ': ' . $entity
				]));
			} else {
				$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.user.singular') . ': ' . $entity);
				$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
					'%entity_label_singular%' => $this->translator->trans('entities.user.singular') . ': ' . $entity
				]));
			}
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$session = $this->container->get('request_stack')->getSession();

		if ($session->get('config')->enablePublic) {
			$dataPanel = FieldGenerator::panel($this->translator->trans('entities.admin.sections.data'))
				->setIcon('fas fa-fw fa-user-secret');
		} else {
			$dataPanel = FieldGenerator::panel($this->translator->trans('entities.user.sections.data'))
				->setIcon('fas fa-fw fa-user');
		}
		$username = FieldGenerator::text('username')
			->setLabel($this->translator->trans('entities.user.fields.username'));
		$email = FieldGenerator::email('email')
			->setLabel($this->translator->trans('entities.user.fields.email'));
		$roles = $this->em->getRepository(Role::class)->findBy(['isAdmin' => true]);
		$rolesIds = array();
		foreach ($roles as $role) {
			if ($this->getUser()->getRole()->isUp($role)) {
				$rolesIds[] = $role->getId();
			}
		}
		$role = FieldGenerator::association('role')
			->setLabel($this->translator->trans('entities.role.singular'))
			->setQueryBuilder(function ($queryBuilder) use ($rolesIds) {
				return $queryBuilder
					->andWhere('entity.id IN (' . implode(',', $rolesIds) . ')');
			})
			->setRequired(true);

		$passwordPanel = FieldGenerator::panel($this->translator->trans('entities.user.sections.password'))
			->setIcon('fas fa-fw fa-key');
		$password = FieldGenerator::repeat('plainPassword')
			->setFormTypeOptions([
				'type' => PasswordType::class,
				'first_options' => [
					'label' => $this->translator->trans('entities.user.fields.password')
				],
				'second_options' => [
					'label' => $this->translator->trans('entities.user.fields.repeatPassword')
				]
			]);

		if ($pageName == Crud::PAGE_INDEX) {
			if ($session->get('config')->enableUsername) {
				yield $username;
			}
			yield $email;
			yield $role;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			if ($session->get('config')->enableUsername) {
				yield $username;
			}
			yield $email;
			yield $role;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			if ($session->get('config')->enableUsername) {
				yield $username;
			}
			yield $email;
			yield $role;
			yield $passwordPanel;
			yield $password->setRequired(true);
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			if ($session->get('config')->enableUsername) {
				yield $username;
			}
			yield $email;
			yield $role;
			yield $passwordPanel;
			yield $password->setRequired(false);
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityAdmin')) {
			$crudAction = filter_input(INPUT_GET, EA::CRUD_ACTION, FILTER_SANITIZE_URL);
			$adminId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
			if (($crudAction == Crud::PAGE_DETAIL || $crudAction == Crud::PAGE_EDIT) && $this->getUser()->getId() == $adminId) {
				$actions->remove(Crud::PAGE_DETAIL, Action::INDEX);
				$actions->remove(Crud::PAGE_DETAIL, Action::DELETE);
				$actions->remove(Crud::PAGE_EDIT, Action::INDEX);
				$actions->remove(Crud::PAGE_EDIT, Action::DELETE);
			} else {
				$actions = Actions::new();
			}
		} else {
			$user = $this->getUser();

			$actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getRole()->getIsAdmin() || ($entity->getRole()->getIsAdmin() && $user->getRole()->isUp($entity->getRole()));
				});
			});
			$actions->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getRole()->getIsAdmin() || ($entity->getRole()->getIsAdmin() && $user->getRole()->isUp($entity->getRole()));
				});
			});
			$actions->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getRole()->getIsAdmin() || ($entity->getRole()->getIsAdmin() && $user->getRole()->isUp($entity->getRole()));
				});
			});
			$actions->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getRole()->getIsAdmin() || ($entity->getRole()->getIsAdmin() && $user->getRole()->isUp($entity->getRole()));
				});
			});
			$actions->update(Crud::PAGE_EDIT, Action::DELETE, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getRole()->getIsAdmin() || ($entity->getRole()->getIsAdmin() && $user->getRole()->isUp($entity->getRole()));
				});
			});
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}

	public function edit(\EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext $context)
	{
		$redirect = parent::edit($context);
		if ($redirect instanceof \Symfony\Component\HttpFoundation\RedirectResponse) {
			if (!$this->getUser()->hasPermission('entityAdmin')) {
				$url = $this->adminUrlGenerator
					->setRoute('admin_home')
					->generateUrl();
				return $this->redirect($url);
			}
		}
		return $redirect;
	}

	public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
	{
		$response = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters)
			->leftJoin('entity.role', 'r')
			->andWhere("r.isAdmin = true");

		return $response;
	}

	public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $keyValueStore, AdminContext $context): FormBuilderInterface
	{
		$session = $this->container->get('request_stack')->getSession();

		$formBuilder = parent::createEditFormBuilder($entityDto, $keyValueStore, $context);
		$this->addEncodePasswordEventListener($formBuilder);
		if (!$session->get('config')->enableUsername) {
			$this->addUsernameEventListener($formBuilder);
		}

		return $formBuilder;
	}

	public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
	{
		$session = $this->container->get('request_stack')->getSession();

		$formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
		$this->addEncodePasswordEventListener($formBuilder);
		if (!$session->get('config')->enableUsername) {
			$this->addUsernameEventListener($formBuilder);
		}

		return $formBuilder;
	}

	public function addEncodePasswordEventListener(FormBuilderInterface $formBuilder)
	{
		$formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
			$user = $event->getData();
			if ($user->getPlainPassword()) {
				$user->setPassword($this->passwordHasher->hashPassword($user, $user->getPlainPassword()));
			}
		});
	}

	public function addUsernameEventListener(FormBuilderInterface $formBuilder)
	{
		$formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
			$user = $event->getData();
			$user->setUsername($user->getEmail());
		});
	}

	public function exportAction(Request $request)
	{
		$session = $this->container->get('request_stack')->getSession();
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		if ($session->get('config')->enablePublic) {
			$entityName = $this->translator->trans('entities.admin.plural');
		} else {
			$entityName = $this->translator->trans('entities.user.plural');
		}
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}
}
