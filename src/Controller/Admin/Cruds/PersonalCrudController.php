<?php

namespace App\Controller\Admin\Cruds;

use App\Controller\Admin\Filters\DepartamentoFilter;
use App\Entity\Personal;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class PersonalCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return Personal::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.personal.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.personal.plural'));

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.personal.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.personal.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.personal.sections.data'))
			->setIcon('fas fa-fw fa-person');
		$fullname = FieldGenerator::integer('fullname')
			->setLabel($this->translator->trans('entities.personal.fields.fullname'));
		$name = FieldGenerator::text('name')
			->setLabel($this->translator->trans('entities.personal.fields.name'))
			->setColumns(4);
		$lastname = FieldGenerator::text('lastname')
			->setLabel($this->translator->trans('entities.personal.fields.lastname'))
			->setColumns(8);
		$puestos = FieldGenerator::association('puestos')
			->setLabel($this->translator->trans('entities.puesto.plural'))
			->setTemplatePath('field/extendedMany.html.twig')
			->setQueryBuilder(function ($queryBuilder) {
				return $queryBuilder
				->leftJoin('entity.despacho', 'd')
				->leftJoin('d.planta', 'p')
				->leftJoin('p.sede', 's')
				->addOrderBy("s.orden", "ASC")
				->addOrderBy("p.orden", "ASC")
				->addOrderBy("d.orden", "ASC")
				->addOrderBy("entity.orden", "ASC");
			});

		if ($pageName == Crud::PAGE_INDEX) {
			yield $fullname;
			yield $puestos;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $name;
			yield $lastname;
			yield $puestos;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $name;
			yield $lastname;
			yield $puestos;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $name;
			yield $lastname;
			yield $puestos;
		}
	}
    
    public function configureFilters(Filters $filters): Filters
    {
        $filters->add(DepartamentoFilter::new('departamento', $this->translator->trans('entities.departamento.singular')));

        return $filters;
    }

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityPersonal')) {
			$actions = Actions::new();
		}

		$actions->add(
			Crud::PAGE_INDEX,
			Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
				->setIcon('icon ti ti-download')
				->linkToCrudAction('exportAction')
				->createAsGlobalAction()
		);

		return $actions;
	}

	public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
	{
		$response = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters)
			->leftJoin('entity.puestos', 'pu')
			->leftJoin('pu.despacho', 'd')
			->leftJoin('d.planta', 'p')
			->leftJoin('p.sede', 's')
			->groupBy('entity.id')
			->orderBy("s.orden", "ASC")
			->addOrderBy("p.orden", "ASC")
			->addOrderBy("d.orden", "ASC");
		return $response;
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.personal.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}
}
