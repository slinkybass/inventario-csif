<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\TipoInventario;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;

class TipoInventarioCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return TipoInventario::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.tipoInventario.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.tipoInventario.plural'));
		$crud->setDefaultSort(['categoriaInventario.name' => 'ASC', 'orden' => 'ASC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.tipoInventario.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.tipoInventario.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.tipoInventario.sections.data'))
			->setIcon('fas fa-fw fa-table-cells');
		$orden = FieldGenerator::integer('orden')
			->setLabel($this->translator->trans('entities.tipoInventario.fields.orden'))
			->setColumns(1);
		$name = FieldGenerator::text('name')
			->setLabel($this->translator->trans('entities.tipoInventario.fields.name'))
			->setColumns(7);
		$categoriaInventario = FieldGenerator::association('categoriaInventario')
			->setLabel($this->translator->trans('entities.categoriaInventario.singular'))
			->setColumns(4);
		$elementosPuesto = FieldGenerator::switch('elementosPuesto')
			->setLabel($this->translator->trans('entities.tipoInventario.fields.elementosPuesto'))
			->setColumns(4);
		$hasExtension = FieldGenerator::switch('hasExtension')
			->setLabel($this->translator->trans('entities.tipoInventario.fields.hasExtension'))
			->setColumns(4);
		$hasLicenses = FieldGenerator::switch('hasLicenses')
			->setLabel($this->translator->trans('entities.tipoInventario.fields.hasLicenses'))
			->setColumns(4);
		$hasRemote = FieldGenerator::switch('hasRemote')
			->setLabel($this->translator->trans('entities.tipoInventario.fields.hasRemote'))
			->setColumns(4);
		$hasIP = FieldGenerator::switch('hasIP')
			->setLabel($this->translator->trans('entities.tipoInventario.fields.hasIP'))
			->setColumns(4);
		$hasGateway = FieldGenerator::switch('hasGateway')
			->setLabel($this->translator->trans('entities.tipoInventario.fields.hasGateway'))
			->setColumns(4);
		$elementosInventario = FieldGenerator::association('elementosInventario')
			->setLabel($this->translator->trans('entities.elementoInventario.plural'));

		if ($pageName == Crud::PAGE_INDEX) {
			yield $name;
			yield $categoriaInventario;
			yield $elementosInventario;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $name;
			yield $categoriaInventario;
			yield $elementosPuesto;
			yield $hasExtension;
			yield $hasLicenses;
			yield $hasRemote;
			yield $hasIP;
			yield $hasGateway;
			yield $elementosInventario->setTemplatePath('field/extendedMany.html.twig');
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $orden;
			yield $name;
			yield $categoriaInventario;
			yield $elementosPuesto;
			yield $hasExtension;
			yield $hasLicenses;
			yield $hasRemote;
			yield $hasIP;
			yield $hasGateway;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $orden;
			yield $name;
			yield $categoriaInventario;
			yield $elementosPuesto;
			yield $hasExtension;
			yield $hasLicenses;
			yield $hasRemote;
			yield $hasIP;
			yield $hasGateway;
		}
	}
    
    public function configureFilters(Filters $filters): Filters
    {
        $filters->add(EntityFilter::new('categoriaInventario', $this->translator->trans('entities.categoriaInventario.singular')));

        return $filters;
    }

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityTipoInventario')) {
			$actions = Actions::new();
		}

		$actions->add(
			Crud::PAGE_INDEX,
			Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
				->setIcon('icon ti ti-download')
				->linkToCrudAction('exportAction')
				->createAsGlobalAction()
		);

		return $actions;
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.tipoInventario.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}
}
