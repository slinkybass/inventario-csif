<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\Config;
use App\Field\FieldGenerator;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;

class ConfigCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
	}

	public static function getEntityFqcn(): string
	{
		return Config::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.config.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.config.singular'));
		$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.config.singular'));
		$crud->setPageTitle(Crud::PAGE_NEW, $this->translator->trans('entities.config.singular'));
		$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('entities.config.singular'));
		$crud->setSearchFields(null);

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.config.sections.data'))
			->setIcon('fas fa-fw fa-cog');
		$appName = FieldGenerator::text('appName')
			->setLabel($this->translator->trans('entities.config.fields.appName'));
		$appLogo = FieldGenerator::mediaImage('appLogo')
			->setLabel($this->translator->trans('entities.config.fields.appLogo'));
		$appFavicon = FieldGenerator::mediaImage('appFavicon')
			->setLabel($this->translator->trans('entities.config.fields.appFavicon'));
		$locale = FieldGenerator::choice('locale')
			->setLabel($this->translator->trans('entities.config.fields.locale'))
			->setChoices([
				$this->translator->trans('languages.es') => "es",
			]);
		$enableUsername = FieldGenerator::checkbox('enableUsername')
			->setLabel($this->translator->trans('entities.config.fields.enableUsername'));
		$enablePublic = FieldGenerator::checkbox('enablePublic')
			->setLabel($this->translator->trans('entities.config.fields.enablePublic'));

		if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $appName;
			yield $appLogo;
			yield $appFavicon;
			yield $locale;
			yield $enableUsername;
			yield $enablePublic;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $appName;
			yield $appLogo;
			yield $appFavicon;
			yield $locale;
			yield $enableUsername;
			yield $enablePublic;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $appName;
			yield $appLogo;
			yield $appFavicon;
			yield $locale;
			yield $enableUsername;
			yield $enablePublic;
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityConfig')) {
			$actions = Actions::new();
		} else {
			$actions->remove(Crud::PAGE_NEW, Action::INDEX);
			$actions->remove(Crud::PAGE_NEW, Action::SAVE_AND_RETURN);
			$actions->add(Crud::PAGE_NEW, Action::SAVE_AND_CONTINUE);
			$actions->update(Crud::PAGE_NEW, Action::SAVE_AND_CONTINUE, function (Action $action) {
				return $action->setIcon('fas fa-fw fa-save')->addCssClass('btn btn-success action-save btn-loader')->setLabel($this->translator->trans('__ea__action.save'));
			});

			$actions->remove(Crud::PAGE_DETAIL, Action::INDEX);
			$actions->remove(Crud::PAGE_DETAIL, Action::DELETE);

			$actions->remove(Crud::PAGE_EDIT, Action::INDEX);
			$actions->remove(Crud::PAGE_EDIT, Action::DELETE);

			$actions->reorder(Crud::PAGE_NEW, [Action::SAVE_AND_CONTINUE]);
			$actions->reorder(Crud::PAGE_DETAIL, [Action::EDIT]);
			$actions->reorder(Crud::PAGE_EDIT, [Action::SAVE_AND_RETURN]);
		}
		return $actions;
	}

	public function new(\EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext $context)
	{
		$redirect = parent::new($context);
		if ($redirect instanceof \Symfony\Component\HttpFoundation\RedirectResponse) {
			$config = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));

			$url = $this->adminUrlGenerator
				->setAction(Crud::PAGE_DETAIL)
				->setEntityId($config->getId())
				->generateUrl();

			return $this->redirect($url);
		}
		return $redirect;
	}
}
