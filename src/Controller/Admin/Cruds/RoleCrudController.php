<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\Role;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;

class RoleCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return Role::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.role.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.role.plural'));
		$crud->setDefaultSort(['displayName' => 'ASC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.role.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.role.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$session = $this->container->get('request_stack')->getSession();
		$roleId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$role = $roleId ? $this->em->getRepository($this->getEntityFqcn())->find($roleId) : null;

		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.role.sections.data'))
			->setIcon('fas fa-fw fa-lock');
		$displayName = FieldGenerator::text('displayName')
			->setLabel($this->translator->trans('entities.role.fields.displayName'));
		$isAdmin = FieldGenerator::checkbox('isAdmin')
			->setLabel($this->translator->trans('entities.role.fields.isAdmin'))
			->setFormTypeOption('attr.data-hf-parent', 'isAdmin');
		$dataPermissions = FieldGenerator::panel($this->translator->trans('entities.role.sections.dataPermissions'))
			->setIcon('fas fa-fw fa-lock');
		foreach (Role::PERMISSIONS as $k => $v) {
			if ($this->getUser()->hasPermission($k)) {
				$name = $this->translator->trans('entities.role.fields.permissions.' . $k);
				if (!$session->get('config')->enablePublic && $k == 'entityAdmin') {
					$name = $this->translator->trans('entities.role.fields.permissions.' . 'entityUser');
				}
				$permission = FieldGenerator::checkbox('permission_' . $k)
					->setLabel($name)
					->setFormTypeOption('mapped', false)
					->setFormTypeOption('data', $role ? $role->getPermission($k) : $v)
					->setValue($role && $role->getPermission($k) ? true : false)
					->setFormTypeOption('attr.data-hf-child', 'isAdmin');
				if (!$session->get('config')->enablePublic && $k == 'entityUser') {
					$permission->setFormTypeOption('row_attr.class', 'd-none');
				}
				$permissionsArr[] = $permission;
			}
		}

		if ($pageName == Crud::PAGE_INDEX) {
			yield $displayName;
			if ($session->get('config')->enablePublic) {
				yield $isAdmin;
			}
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $displayName;
			if ($session->get('config')->enablePublic) {
				yield $isAdmin;
			}
			yield $dataPermissions;
			foreach ($permissionsArr as $permission) {
				yield $permission;
			}
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $displayName;
			if ($session->get('config')->enablePublic) {
				yield $isAdmin;
			}
			yield $dataPermissions;
			foreach ($permissionsArr as $permission) {
				yield $permission;
			}
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $displayName;
			if ($session->get('config')->enablePublic) {
				yield $isAdmin;
			}
			yield $dataPermissions;
			foreach ($permissionsArr as $permission) {
				yield $permission;
			}
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityRole')) {
			$actions = Actions::new();
		} else {
			$user = $this->getUser();

			$actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getIsAdmin() || ($entity->getIsAdmin() && $user->getRole()->isUp($entity));
				});
			});
			$actions->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getIsAdmin() || ($entity->getIsAdmin() && $user->getRole()->isUp($entity));
				});
			});
			$actions->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getIsAdmin() || ($entity->getIsAdmin() && $user->getRole()->isUp($entity));
				});
			});
			$actions->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getIsAdmin() || ($entity->getIsAdmin() && $user->getRole()->isUp($entity));
				});
			});
			$actions->update(Crud::PAGE_EDIT, Action::DELETE, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getIsAdmin() || ($entity->getIsAdmin() && $user->getRole()->isUp($entity));
				});
			});
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}

	public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $keyValueStore, AdminContext $context): FormBuilderInterface
	{
		$formBuilder = parent::createEditFormBuilder($entityDto, $keyValueStore, $context);
		$this->addIsAdminEventListener($formBuilder);
		$this->addPermissionsEventListener($formBuilder);

		return $formBuilder;
	}

	public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
	{
		$formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
		$this->addIsAdminEventListener($formBuilder);
		$this->addPermissionsEventListener($formBuilder);

		return $formBuilder;
	}

	public function addIsAdminEventListener(FormBuilderInterface $formBuilder)
	{
		$formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
			$session = $this->container->get('request_stack')->getSession();
			if (!$session->get('config')->enablePublic) {
				$role = $event->getData();
				$role->setIsAdmin(true);
			}
		});
	}

	public function addPermissionsEventListener(FormBuilderInterface $formBuilder)
	{
		$formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
			$role = $event->getData();
			$form = $event->getForm();
			foreach (Role::PERMISSIONS as $k => $v) {
				$value = $form->has('permission_' . $k) ? $form->get('permission_' . $k)->getData() : $v;
				$permissions[$k] = $value;
			}
			$role->setPermissions($permissions);
		});
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.role.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}
}
