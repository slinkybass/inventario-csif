<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\ElementoInventario;
use App\Entity\Planta;
use App\Entity\TipoInventario;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Doctrine\ORM\EntityManagerInterface;

class PlantaCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return Planta::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.planta.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.planta.plural'));
		$crud->setDefaultSort(['sede.orden' => 'ASC', 'orden' => 'ASC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.planta.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.planta.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.planta.sections.data'))
			->setIcon('fas fa-fw fa-stairs');
		$orden = FieldGenerator::integer('orden')
			->setLabel($this->translator->trans('entities.planta.fields.orden'))
			->setColumns(1);
		$name = FieldGenerator::text('name')
			->setLabel($this->translator->trans('entities.planta.fields.name'))
			->setColumns(7);
		$sede = FieldGenerator::association('sede')
			->setLabel($this->translator->trans('entities.sede.singular'))
			->setQueryBuilder(function ($queryBuilder) {
				return $queryBuilder
				->addOrderBy("entity.orden", "ASC");
			})
			->setColumns(4);
		$despachos = FieldGenerator::association('despachos')
			->setLabel($this->translator->trans('entities.despacho.plural'));
		$elementosInventario = FieldGenerator::association('elementosInventario')
			->setLabel($this->translator->trans('entities.elementoInventario.plural'))
			->setFormTypeOptionIfNotSet('by_reference', false)
			->setTemplatePath('field/extendedMany.html.twig');

		$dataPanelElementosInventario = FieldGenerator::panel($this->translator->trans('entities.elementoInventario.plural'))
			->setIcon('fas fa-fw fa-inbox');
		$tiposInventarioFields = array();
		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		$slugger = new AsciiSlugger();
		$tis = $this->em->getRepository(TipoInventario::class)->findBy(['elementosPuesto' => false]);
		foreach ($tis as $ti) {
			$eis = $this->em->getRepository(ElementoInventario::class)->findBy(['tipoInventario' => $ti]);
			$choices = array();
			foreach ($eis as $ei) {
				$choices[$ei->__toString()] = $ei->getId();
			}
			$selected = array();
			if ($entity) {
				foreach ($entity->getElementosInventario() as $ei) {
					if ($ei->getTipoInventario() == $ti) {
						$selected[] = $ei->getId();
					}
				}
			}
			if (count($choices)) {
				$tiposInventarioFields[] = FieldGenerator::choiceMultiple($slugger->slug($ti->__toString())->lower())
					->setLabel($ti->__toString())
					->setChoices($choices)
					->setFormTypeOption('mapped', false)
					->setFormTypeOption('data', $selected)
					->setColumns(4);
			}
		}

		if ($pageName == Crud::PAGE_INDEX) {
			yield $name;
			yield $sede;
			yield $despachos;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $name;
			yield $sede;
			yield $despachos->setTemplatePath('field/extendedMany.html.twig');
			yield $elementosInventario;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $orden;
			yield $name;
			yield $sede;
			if (count($tiposInventarioFields)) {
				yield $dataPanelElementosInventario;
			}
			foreach ($tiposInventarioFields as $tipoInventarioField) {
				yield $tipoInventarioField;
			}
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $orden;
			yield $name;
			yield $sede;
			if (count($tiposInventarioFields)) {
				yield $dataPanelElementosInventario;
			}
			foreach ($tiposInventarioFields as $tipoInventarioField) {
				yield $tipoInventarioField;
			}
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityPlanta')) {
			$actions = Actions::new();
		}

		$actions->add(
			Crud::PAGE_INDEX,
			Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
				->setIcon('icon ti ti-download')
				->linkToCrudAction('exportAction')
				->createAsGlobalAction()
		);

		return $actions;
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.planta.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}
}
