<?php

namespace App\Controller\Admin\Cruds;

use App\Controller\Admin\Filters\CategoriaInventarioFilter;
use App\Entity\ElementoInventario;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class ElementoInventarioCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return ElementoInventario::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.elementoInventario.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.elementoInventario.plural'));
		$crud->setDefaultSort(['identificator' => 'ASC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.elementoInventario.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.elementoInventario.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.elementoInventario.sections.data'))
			->setIcon('fas fa-fw fa-inbox');
		$identificator = FieldGenerator::text('identificator')
			->setLabel($this->translator->trans('entities.elementoInventario.fields.identificator'))
			->setColumns(2);
		$name = FieldGenerator::text('name')
			->setLabel($this->translator->trans('entities.elementoInventario.fields.name'))
			->setColumns(5);
		$tipoInventario = FieldGenerator::association('tipoInventario')
			->setLabel($this->translator->trans('entities.tipoInventario.singular'))
			->setColumns(5);
		$categoriaInventarioFilter = $this->getCategoriaInventarioFilter();
		if ($categoriaInventarioFilter) {
			$tipoInventario->setQueryBuilder(function ($queryBuilder) use ($categoriaInventarioFilter) {
				return $queryBuilder->andWhere("entity.categoriaInventario " . $categoriaInventarioFilter['comparison'] . $categoriaInventarioFilter['value']);
			});
		}
		$puesto = FieldGenerator::association('puesto')
			->setLabel($this->translator->trans('entities.puesto.singular'))
			->setColumns(4);
		$despacho = FieldGenerator::association('despacho')
			->setLabel($this->translator->trans('entities.despacho.singular'))
			->setColumns(4);
		$planta = FieldGenerator::association('planta')
			->setLabel($this->translator->trans('entities.planta.singular'))
			->setColumns(4);
		$site = FieldGenerator::text('site')
			->setLabel($this->translator->trans('entities.elementoInventario.fields.site'));

		$dataPanelExtra = FieldGenerator::panel($this->translator->trans('entities.elementoInventario.sections.dataExtra'))
			->setIcon('fas fa-fw fa-inbox');
		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		$remote = FieldGenerator::switch('remote')
			->setLabel($this->translator->trans('entities.elementoInventario.fields.remote'))
			->setColumns(12);
		$extension = FieldGenerator::text('extension')
			->setLabel($this->translator->trans('entities.elementoInventario.fields.extension'))
			->setColumns(4);
		$ip = FieldGenerator::mask('ip')
			->setLabel($this->translator->trans('entities.elementoInventario.fields.ip'))
			->setFormTypeOption('attr.mask', '0[00].0[00].0[00].0[00]')
			->setColumns(4);
		$gateway = FieldGenerator::mask('gateway')
			->setLabel($this->translator->trans('entities.elementoInventario.fields.gateway'))
			->setFormTypeOption('attr.mask', '0[00].0[00].0[00].0[00]')
			->setColumns(4);
		$licenses = FieldGenerator::association('licenses')
			->setLabel($this->translator->trans('entities.license.plural'))
			->setFormTypeOptionIfNotSet('by_reference', false)
			->setTemplatePath('field/extendedMany.html.twig')
			->setColumns(4);
		$extraInfo = FieldGenerator::text('extraInfo')
			->setLabel($this->translator->trans('entities.elementoInventario.fields.extraInfo'))
			->setTemplatePath('field/html.html.twig');

		if ($pageName == Crud::PAGE_INDEX) {
			yield $tipoInventario;
			yield $identificator;
			yield $name;
			yield $site;
			yield $extraInfo;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $identificator;
			yield $name;
			yield $tipoInventario;
			yield $site;
			if ($entity && (
				$entity->getTipoInventario()->getHasExtension() || 
				$entity->getTipoInventario()->getHasRemote() || 
				$entity->getTipoInventario()->getHasIp() || 
				$entity->getTipoInventario()->getHasGateway() || 
				$entity->getTipoInventario()->getHasLicenses()
			)) {
				yield $dataPanelExtra;
			}
			if ($entity && $entity->getTipoInventario()->getHasRemote()) {
				yield $remote;
			}
			if ($entity && $entity->getTipoInventario()->getHasExtension()) {
				yield $extension;
			}
			if ($entity && $entity->getTipoInventario()->getHasIp()) {
				yield $ip;
			}
			if ($entity && $entity->getTipoInventario()->getHasGateway()) {
				yield $gateway;
			}
			if ($entity && $entity->getTipoInventario()->getHasLicenses()) {
				yield $licenses;
			}
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $identificator;
			yield $name;
			yield $tipoInventario;
			yield $puesto;
			yield $despacho;
			yield $planta;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $identificator;
			yield $name;
			yield $tipoInventario;
			yield $puesto;
			yield $despacho;
			yield $planta;
			if ($entity && (
				$entity->getTipoInventario()->getHasExtension() || 
				$entity->getTipoInventario()->getHasRemote() || 
				$entity->getTipoInventario()->getHasIp() || 
				$entity->getTipoInventario()->getHasGateway() || 
				$entity->getTipoInventario()->getHasLicenses()
			)) {
				yield $dataPanelExtra;
			}
			if ($entity && $entity->getTipoInventario()->getHasRemote()) {
				yield $remote;
			}
			if ($entity && $entity->getTipoInventario()->getHasExtension()) {
				yield $extension;
			}
			if ($entity && $entity->getTipoInventario()->getHasIp()) {
				yield $ip;
			}
			if ($entity && $entity->getTipoInventario()->getHasGateway()) {
				yield $gateway;
			}
			if ($entity && $entity->getTipoInventario()->getHasLicenses()) {
				yield $licenses;
			}
		}
	}
    
    public function configureFilters(Filters $filters): Filters
    {
        $filters->add(CategoriaInventarioFilter::new('categoriaInventario', $this->translator->trans('entities.categoriaInventario.singular')));
		$tipoInventarioFilter = EntityFilter::new('tipoInventario', $this->translator->trans('entities.tipoInventario.singular'));
		$categoriaInventarioFilter = $this->getCategoriaInventarioFilter();
		if ($categoriaInventarioFilter) {
			$tipoInventarioFilter->setFormTypeOption('value_type_options', [
				'query_builder' => function ($repository) use ($categoriaInventarioFilter) {
					return $repository->createQueryBuilder('e')->andWhere('e.categoriaInventario' . $categoriaInventarioFilter['comparison'] . $categoriaInventarioFilter['value']);
				}
			]);
		}
        $filters->add($tipoInventarioFilter);

        return $filters;
    }

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityElementoInventario')) {
			$actions = Actions::new();
		}

		$actions->add(
			Crud::PAGE_INDEX,
			Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
				->setIcon('icon ti ti-download')
				->linkToCrudAction('exportAction')
				->createAsGlobalAction()
		);

		return $actions;
	}

	public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
	{
		$response = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters)
			->leftJoin('entity.tipoInventario', 'ti')
			->leftJoin('entity.puesto', 'pu')
			->leftJoin('entity.despacho', 'd')
			->leftJoin('entity.planta', 'pl')
			->leftJoin('pu.despacho', 'pu_d')
			->leftJoin('pu_d.planta', 'pu_d_pl')
			->leftJoin('pu_d_pl.sede', 'pu_d_pl_s')
			->leftJoin('d.planta', 'd_pl')
			->leftJoin('d_pl.sede', 'd_pl_s')
			->leftJoin('pl.sede', 'pl_s')
			->orderBy("pl_s.orden", "ASC")
			->addOrderBy("pl.orden", "ASC")
			->addOrderBy("d_pl_s.orden", "ASC")
			->addOrderBy("d_pl.orden", "ASC")
			->addOrderBy("d.orden", "ASC")
			->addOrderBy("pu_d_pl_s.orden", "ASC")
			->addOrderBy("pu_d_pl.orden", "ASC")
			->addOrderBy("pu_d.orden", "ASC")
			->addOrderBy("pu.orden", "ASC")
			->addOrderBy("ti.orden", "ASC");

		return $response;
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.elementoInventario.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}

	public function getCategoriaInventarioFilter()
	{
		$filtersURL = filter_input(INPUT_GET, EA::FILTERS, FILTER_SANITIZE_URL, FILTER_REQUIRE_ARRAY);
		if ($filtersURL && array_key_exists('categoriaInventario', $filtersURL) && array_key_exists('comparison', $filtersURL['categoriaInventario']) && array_key_exists('value', $filtersURL['categoriaInventario'])) {
			return [
				'comparison' => $filtersURL['categoriaInventario']['comparison'],
				'value' => $filtersURL['categoriaInventario']['value'],
			];
		}
		return null;
	}

	public function new(\EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext $context)
	{
		$redirect = parent::new($context);
		if ($redirect instanceof \Symfony\Component\HttpFoundation\RedirectResponse) {
			if ($context->getEntity()->getInstance()->getTipoInventario()->getHasExtension() || 
				$context->getEntity()->getInstance()->getTipoInventario()->getHasRemote() || 
				$context->getEntity()->getInstance()->getTipoInventario()->getHasIp() || 
				$context->getEntity()->getInstance()->getTipoInventario()->getHasGateway() || 
				$context->getEntity()->getInstance()->getTipoInventario()->getHasLicenses()
			) {
				$url = $this->adminUrlGenerator
					->setAction(Crud::PAGE_EDIT)
					->setEntityId($context->getEntity()->getInstance()->getId())
					->generateUrl();

				return $this->redirect($url);
			}
		}
		return $redirect;
	}
}
