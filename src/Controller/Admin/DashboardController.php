<?php

namespace App\Controller\Admin;

use App\Entity\CategoriaInventario;
use App\Entity\Config;
use App\Entity\Departamento;
use App\Entity\Despacho;
use App\Entity\ElementoInventario;
use App\Entity\License;
use App\Entity\Personal;
use App\Entity\Planta;
use App\Entity\Puesto;
use App\Entity\Sede;
use App\Entity\TipoInventario;
use App\Entity\Role;
use App\Entity\User;

use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;

class DashboardController extends AbstractDashboardController
{
	private $em;
	private $translator;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator)
	{
		$this->em = $em;
		$this->translator = $translator;
	}

	/**
	 * @Route("/admin", name="admin")
	 */
	public function index(): Response
	{
		$routeBuilder = $this->container->get(AdminUrlGenerator::class);

		return $this->redirect($routeBuilder->setRoute('admin_home')->generateUrl());
	}

	public function configureDashboard(): Dashboard
	{
		$session = $this->container->get('request_stack')->getSession();

		$dashboard = Dashboard::new();

		$dashboard->setTitle('<img src="' . $session->get('config')->appLogo . '" class="mx-auto d-block">');
		$dashboard->setFaviconPath($session->get('config')->appFavicon);

		return $dashboard;
	}

	public function configureAssets(): Assets
	{
		$assets = Assets::new();

		$assets->addWebpackEncoreEntry('app');
		$assets->addWebpackEncoreEntry('admin');

		return $assets;
	}

	public function configureCrud(): Crud
	{
		$crud = Crud::new();

		$crud->setDefaultSort(['id' => 'DESC']);

		$crud->setPageTitle(Crud::PAGE_INDEX, $this->translator->trans('ea.titles.list'));
		$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('ea.titles.detail'));
		$crud->setPageTitle(Crud::PAGE_NEW, $this->translator->trans('ea.titles.new'));
		$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit'));

		return $crud;
	}

	public function configureMenuItems(): iterable
	{
		$config = $this->em->getRepository(Config::class)->findOneBy(array(), array('id' => 'DESC'));
		$session = $this->container->get('request_stack')->getSession();

		yield MenuItem::linkToRoute($this->translator->trans('ea.home.title'), 'fas fa-fw fa-home', 'admin_home');
		if ($this->getUser()->hasPermission('entityMedia')) {
			yield MenuItem::linkToRoute($this->translator->trans('ea.media.title'), 'fas fa-fw fa-file', 'admin_media');
		}

		$organizationItems = array();
		if ($this->getUser()->hasPermission('entitySede')) {
			$organizationItems[] = MenuItem::linkToCrud($this->translator->trans('entities.sede.plural'), 'fas fa-fw fa-building', Sede::class);
		}
		if ($this->getUser()->hasPermission('entityPlanta')) {
			$organizationItems[] = MenuItem::linkToCrud($this->translator->trans('entities.planta.plural'), 'fas fa-fw fa-stairs', Planta::class);
		}
		if ($this->getUser()->hasPermission('entityDepartamento')) {
			$organizationItems[] = MenuItem::linkToCrud($this->translator->trans('entities.departamento.plural'), 'fas fa-fw fa-box-archive', Departamento::class);
		}
		if ($this->getUser()->hasPermission('entityDespacho')) {
			$organizationItems[] = MenuItem::linkToCrud($this->translator->trans('entities.despacho.plural'), 'fas fa-fw fa-door-closed', Despacho::class);
		}
		if ($this->getUser()->hasPermission('entityPuesto')) {
			$organizationItems[] = MenuItem::linkToCrud($this->translator->trans('entities.puesto.plural'), 'fas fa-fw fa-chair', Puesto::class);
		}
		if (count($organizationItems) == 1) {
			yield $organizationItems[0];
		} elseif (count($organizationItems) > 1) {
			yield MenuItem::subMenu($this->translator->trans('ea.menu.organization'), 'fas fa-fw fa-folder-tree')->setSubItems($organizationItems);
		}

		$inventoryItems = array();
		if ($this->getUser()->hasPermission('entityCategoriaInventario')) {
			$inventoryItems[] = MenuItem::linkToCrud($this->translator->trans('entities.categoriaInventario.plural'), 'fas fa-fw fa-table-cells-large', CategoriaInventario::class);
		}
		if ($this->getUser()->hasPermission('entityTipoInventario')) {
			$inventoryItems[] = MenuItem::linkToCrud($this->translator->trans('entities.tipoInventario.plural'), 'fas fa-fw fa-table-cells', TipoInventario::class);
		}
		if ($this->getUser()->hasPermission('entityElementoInventario')) {
			$categoriasInventario = $this->em->getRepository(CategoriaInventario::class)->findBy(array(), array('name' => 'ASC'));
			foreach ($categoriasInventario as $categoriaInventario) {
				$link = MenuItem::linkToCrud($categoriaInventario->__toString(), 'fas fa-fw fa-inbox', ElementoInventario::class);
				$link->setQueryParameter("filters", [
					"categoriaInventario" => [
						"comparison" => "=",
						"value" => $categoriaInventario->getId()
					]
				]);
				$inventoryItems[] = $link;
			}
		}
		if ($this->getUser()->hasPermission('entityLicense')) {
			$inventoryItems[] = MenuItem::linkToCrud($this->translator->trans('entities.license.plural'), 'fas fa-fw fa-list', License::class);
		}
		if (count($inventoryItems) == 1) {
			yield $inventoryItems[0];
		} elseif (count($inventoryItems) > 1) {
			yield MenuItem::subMenu($this->translator->trans('ea.menu.inventory'), 'fas fa-fw fa-boxes-stacked')->setSubItems($inventoryItems);
		}

		if ($this->getUser()->hasPermission('entityPersonal')) {
			yield MenuItem::linkToCrud($this->translator->trans('entities.personal.plural'), 'fas fa-fw fa-person', Personal::class);
		}

		$userItems = array();
		if ($session->get('config')->enablePublic && $this->getUser()->hasPermission('entityUser')) {
			$userItems[] = MenuItem::linkToCrud($this->translator->trans('entities.user.plural'), 'fas fa-fw fa-user', User::class)->setController(Cruds\UserCrudController::class);
		}
		if ($this->getUser()->hasPermission('entityAdmin')) {
			if ($session->get('config')->enablePublic) {
				$userItems[] = MenuItem::linkToCrud($this->translator->trans('entities.admin.plural'), 'fas fa-fw fa-user-secret', User::class)->setController(Cruds\AdminCrudController::class);
			} else {
				$userItems[] = MenuItem::linkToCrud($this->translator->trans('entities.user.plural'), 'fas fa-fw fa-user', User::class)->setController(Cruds\AdminCrudController::class);
			}
		}
		if ($this->getUser()->hasPermission('entityRole')) {
			$userItems[] = MenuItem::linkToCrud($this->translator->trans('entities.role.plural'), 'fas fa-fw fa-lock', Role::class);
		}
		if (count($userItems) == 1) {
			yield $userItems[0];
		} elseif (count($userItems) > 1) {
			yield MenuItem::subMenu($this->translator->trans('entities.user.plural'), 'fas fa-fw fa-users')->setSubItems($userItems);
		}

		if ($this->getUser()->hasPermission('entityConfig')) {
			$configLink = MenuItem::linkToCrud($this->translator->trans('entities.config.singular'), 'fas fa-fw fa-cog', Config::class);
			$configLink = $config ? $configLink->setAction(Crud::PAGE_DETAIL)->setEntityId($config->getId()) : $configLink->setAction(Crud::PAGE_NEW);
			yield $configLink;
		}
	}

	public function configureUserMenu(UserInterface $user): UserMenu
	{
		$userMenu = parent::configureUserMenu($user);

		$menuItems = array();
		$menuItems[] = MenuItem::linkToCrud($this->translator->trans('ea.profile.title'), 'icon ti ti-user-circle', User::class)
			->setController(Cruds\AdminCrudController::class)
			->setAction(Crud::PAGE_EDIT)
			->setEntityId($user->getId());
		$menuItems[] = MenuItem::section();
		$menuItems[] = MenuItem::linkToLogout('__ea__user.sign_out', 'icon ti ti-logout');
		$userMenu->setMenuItems($menuItems);

		return $userMenu;
	}

	public function configureActions(): Actions
	{
		$actions = Actions::new();

		$actions->addBatchAction(Action::BATCH_DELETE);
		$actions->update(Crud::PAGE_INDEX, Action::BATCH_DELETE, function (Action $action) {return $action->setIcon('icon ti ti-trash')->setCssClass('action-batchDelete btn btn-danger pr-0 text-white');});

		$actions->add(Crud::PAGE_INDEX, Action::NEW);
		$actions->add(Crud::PAGE_INDEX, Action::DETAIL);
		$actions->add(Crud::PAGE_INDEX, Action::EDIT);
		$actions->add(Crud::PAGE_INDEX, Action::DELETE);
		$actions->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {return $action->setIcon('icon ti ti-plus');});
		$actions->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {return $action->setIcon('icon ti ti-eye');});
		$actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {return $action->setIcon('icon ti ti-edit');});
		$actions->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {return $action->setIcon('icon ti ti-trash');});

		$actions->add(Crud::PAGE_NEW, Action::INDEX);
		$actions->add(Crud::PAGE_NEW, Action::SAVE_AND_RETURN);
		$actions->update(Crud::PAGE_NEW, Action::INDEX, function (Action $action) {return $action->setIcon('icon ti ti-chevron-left');});
		$actions->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {return $action->setIcon('icon ti ti-device-floppy')->addCssClass('btn-success text-white');});

		$actions->add(Crud::PAGE_DETAIL, Action::INDEX);
		$actions->add(Crud::PAGE_DETAIL, Action::DELETE);
		$actions->add(Crud::PAGE_DETAIL, Action::EDIT);
		$actions->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {return $action->setIcon('icon ti ti-chevron-left');});
		$actions->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) {return $action->setIcon('icon ti ti-trash')->setCssClass('action-delete btn btn-danger pr-0 text-white');});
		$actions->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {return $action->setIcon('icon ti ti-edit');});

		$actions->add(Crud::PAGE_EDIT, Action::INDEX);
		$actions->add(Crud::PAGE_EDIT, Action::DELETE);
		$actions->add(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN);
		$actions->update(Crud::PAGE_EDIT, Action::INDEX, function (Action $action) {return $action->setIcon('icon ti ti-chevron-left');});
		$actions->update(Crud::PAGE_EDIT, Action::DELETE, function (Action $action) {return $action->setIcon('icon ti ti-trash')->setCssClass('action-delete btn btn-danger pr-0 text-white');});
		$actions->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {return $action->setIcon('icon ti ti-device-floppy')->addCssClass('btn-success text-white');});
			
		$actions->reorder(Crud::PAGE_INDEX, [Action::DETAIL, Action::EDIT, Action::DELETE]);
		$actions->reorder(Crud::PAGE_NEW, [Action::INDEX, Action::SAVE_AND_RETURN]);
		$actions->reorder(Crud::PAGE_DETAIL, [Action::INDEX, Action::DELETE, Action::EDIT]);
		$actions->reorder(Crud::PAGE_EDIT, [Action::INDEX, Action::DELETE, Action::SAVE_AND_RETURN]);

		return $actions;
	}
}
