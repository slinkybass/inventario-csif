<?php

namespace App\Repository;

use App\Entity\CategoriaInventario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategoriaInventario|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoriaInventario|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoriaInventario[]	findAll()
 * @method CategoriaInventario[]	findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriaInventarioRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, CategoriaInventario::class);
	}

	// /**
	//  * @return CategoriaInventario[] Returns an array of CategoriaInventario objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('c.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?CategoriaInventario
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
