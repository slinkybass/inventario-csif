<?php

namespace App\Repository;

use App\Entity\ElementoInventario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ElementoInventario|null find($id, $lockMode = null, $lockVersion = null)
 * @method ElementoInventario|null findOneBy(array $criteria, array $orderBy = null)
 * @method ElementoInventario[]	findAll()
 * @method ElementoInventario[]	findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ElementoInventarioRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, ElementoInventario::class);
	}

	// /**
	//  * @return ElementoInventario[] Returns an array of ElementoInventario objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('e')
			->andWhere('e.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('e.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?ElementoInventario
	{
		return $this->createQueryBuilder('e')
			->andWhere('e.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
