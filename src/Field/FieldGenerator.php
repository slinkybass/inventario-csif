<?php

namespace App\Field;

use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\HiddenField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class FieldGenerator extends AbstractType
{
	public static function panel(string $name)
	{
		return FormField::addPanel($name)
			->setColumns(12);
	}

	public static function id(string $name)
	{
		return IdField::new($name)
			->setColumns(12);
	}

	public static function text(string $name)
	{
		return TextField::new($name)
			->setColumns(12);
	}

	public static function mask(string $name)
	{
		return TextField::new($name)
			->setFormTypeOption('attr.data-ea-mask-field', 'true')
			->addWebpackEncoreEntries('form-type-mask')
			->setColumns(12);
	}

	public static function hidden(string $name)
	{
		return HiddenField::new($name)
			->setColumns(12);
	}

	public static function slug(string $name)
	{
		return SlugField::new($name)
			->setColumns(12);
	}

	public static function textarea(string $name)
	{
		return TextareaField::new($name)
			->setColumns(12);
	}

	public static function texteditor(string $name)
	{
		return TextEditorField::new($name)
			->setTemplateName('crud/field/text')
			->setFormTypeOption('block_prefix', 'texteditor')
			->setFormTypeOption('attr.data-ea-texteditor-field', 'true')
			->addWebpackEncoreEntries('form-type-texteditor')
			->setColumns(12);
	}

	public static function codeeditor(string $name)
	{
		return CodeEditorField::new($name)
			->setColumns(12);
	}

	public static function choice(string $name)
	{
		return ChoiceField::new($name)
			->addWebpackEncoreEntries('form-type-select')
			->setColumns(12);
	}

	public static function choiceMultiple(string $name)
	{
		return ChoiceField::new($name)
			->addWebpackEncoreEntries('form-type-select')
			->allowMultipleChoices(true)
			->setColumns(12);
	}

	public static function choiceExpanded(string $name)
	{
		return ChoiceField::new($name)
			->renderExpanded(true)
			->setFormTypeOption('placeholder', '-')
			->setColumns(12);
	}

	public static function choiceExpandedMultiple(string $name)
	{
		return ChoiceField::new($name)
			->renderExpanded(true)
			->allowMultipleChoices(true)
			->setColumns(12);
	}

	public static function checkbox(string $name)
	{
		return BooleanField::new($name)
			->renderAsSwitch(false)
			->setColumns(12);
	}

	public static function switch(string $name)
	{
		return BooleanField::new($name)
			->setColumns(12);
	}

	public static function email(string $name)
	{
		return EmailField::new($name)
			->setColumns(12);
	}

	public static function phone(string $name)
	{
		return TelephoneField::new($name)
			->setColumns(12);
	}

	public static function url(string $name)
	{
		return UrlField::new($name)
			->setColumns(12);
	}

	public static function date(string $name)
	{
		return DateField::new($name)
			->setFormTypeOption('attr.data-ea-date-field', 'true')
			->addWebpackEncoreEntries('form-type-date')
			->setColumns(12);
	}

	public static function datetime(string $name)
	{
		return DateTimeField::new($name)
			->setFormTypeOption('attr.data-ea-datetime-field', 'true')
			->addWebpackEncoreEntries('form-type-datetime')
			->setColumns(12);
	}

	public static function time(string $name)
	{
		return TimeField::new($name)
			->setFormTypeOption('attr.data-ea-time-field', 'true')
			->addWebpackEncoreEntries('form-type-time')
			->setColumns(12);
	}

	public static function repeat(string $name)
	{
		return TextField::new($name)
			->setFormType(RepeatedType::class)
			->setColumns(12);
	}

	public static function integer(string $name)
	{
		return IntegerField::new($name)
			->setColumns(12);
	}

	public static function float(string $name)
	{
		return NumberField::new($name)
			->setFormTypeOption('html5', true)
			->setFormTypeOption('attr.step', '.01')
			->setColumns(12);
	}

	public static function money(string $name)
	{
		return MoneyField::new($name)
			->setCurrency('EUR')
			->setColumns(12);
	}

	public static function slider(string $name)
	{
		return IntegerField::new($name)
			->setFormTypeOption('block_prefix', 'slider')
			->setFormTypeOption('attr.data-ea-slider-field', 'true')
			->addWebpackEncoreEntries('form-type-slider')
			->setColumns(12);
	}

	public static function sliderFloat(string $name)
	{
		return NumberField::new($name)
			->setFormTypeOption('html5', true)
			->setFormTypeOption('attr.step', '.01')
			->setFormTypeOption('block_prefix', 'slider')
			->setFormTypeOption('attr.data-ea-slider-field', 'true')
			->addWebpackEncoreEntries('form-type-slider')
			->setColumns(12);
	}

	public static function color(string $name)
	{
		return ColorField::new($name)
			->setFormTypeOption('block_prefix', 'colorselector')
			->setFormTypeOption('attr.data-ea-color-field', 'true')
			->addWebpackEncoreEntries('form-type-color')
			->setColumns(12);
	}

	public static function array(string $name)
	{
		return ArrayField::new($name)
			->setColumns(12);
	}

	public static function collection(string $name)
	{
		return CollectionField::new($name)
			->setColumns(12);
	}

	public static function association(string $name)
	{
		return AssociationField::new($name)
			->addWebpackEncoreEntries('form-type-select')
			->setColumns(12);
	}

	public static function media(string $name)
	{
		return TextField::new($name)
			->setTemplatePath('field/media.html.twig')
			->setFormTypeOption('block_prefix', 'media')
			->addWebpackEncoreEntries('form-type-media')
			->setFormTypeOption('attr.data-image', 'false')
			->setFormTypeOption('attr.data-type', 'public_all')
			->setColumns(12);
	}

	public static function mediaImage(string $name)
	{
		return TextField::new($name)
			->setTemplatePath('field/media.html.twig')
			->setFormTypeOption('block_prefix', 'media')
			->addWebpackEncoreEntries('form-type-media')
			->setFormTypeOption('attr.data-image', 'true')
			->setFormTypeOption('attr.data-type', 'public_images')
			->setColumns(12);
	}
}
