<?php

namespace App\Service;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CsvService
{
	public function export($data, $filename, $delimiter_key = ";", $options = ['output_utf8_bom' => true])
	{
		$serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder([CsvEncoder::DELIMITER_KEY => $delimiter_key])]);
		$response = new Response($serializer->encode($data, CsvEncoder::FORMAT, $options));
		$response->headers->set('Content-Type', 'text/csv');
		$response->headers->set('Content-Disposition', "attachment; filename=\"$filename\"");
		return $response;
	}

	public function getEntityAsData($entities, FieldCollection $fields)
	{
		$propertyAccessor = PropertyAccess::createPropertyAccessor();
		$fieldNames = iterator_to_array($fields->getIterator());

		$data = [];
		foreach ($entities as $entity) {
			$entityArr = [];
			foreach ($fieldNames as $field) {
				$value = $propertyAccessor->getValue($entity, $field->getProperty());
				$value = $value instanceof \DateTime ? $value->format('Y-m-d H:i:s') : $value;
				$entityArr[$field->getProperty()] = $value;
			}
			$data[] = $entityArr;
		}

		return $data;
	}
}