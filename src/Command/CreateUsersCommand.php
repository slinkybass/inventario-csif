<?php

namespace App\Command;

use App\Entity\Role;
use App\Entity\User;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

use Doctrine\ORM\EntityManagerInterface;

class CreateUsersCommand extends Command
{
	private $em;
	private $passwordHasher;

	public function __construct(EntityManagerInterface $em, UserPasswordHasherInterface $passwordHasher)
	{
		$this->em = $em;
		$this->passwordHasher = $passwordHasher;

		parent::__construct();
	}

	protected static $defaultName = 'app:create-users';

	protected function configure(): void
	{
		$this->setHelp("This command allows you to create the initial users.");
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$this->roles($output);
		$this->users($output);

		return Command::SUCCESS;
	}

	private function roles(OutputInterface $output): void
	{
		$roleSuperAdminExists = $this->em->getRepository(Role::class)->findOneBy([
			'name' => "ROLE_SUPERADMIN"
		]);
		if (!$roleSuperAdminExists) {
			$roleSuperAdmin = new Role();
			$roleSuperAdmin->setDisplayName('Superadmin');
			$roleSuperAdmin->setName('ROLE_SUPERADMIN');
			$roleSuperAdmin->setIsAdmin(true);
			$permissions = array();
			foreach (Role::PERMISSIONS as $k => $v) {
				$permissions[$k] = true;
			}
			$roleSuperAdmin->setPermissions($permissions);
			$this->em->persist($roleSuperAdmin);
			$output->writeln('<bg=green;options=bold>CREATED ROLE ROLE_SUPERADMIN</>');
		} else {
			$output->writeln('<bg=red;options=bold>ROLE ROLE_SUPERADMIN ALREADY EXISTS</>');
		}

		$roleAdminExists = $this->em->getRepository(Role::class)->findOneBy([
			'name' => "ROLE_ADMIN"
		]);
		if (!$roleAdminExists) {
			$roleAdmin = new Role();
			$roleAdmin->setDisplayName('Admin');
			$roleAdmin->setName('ROLE_ADMIN');
			$roleAdmin->setIsAdmin(true);
			$this->em->persist($roleAdmin);
			$output->writeln('<bg=green;options=bold>CREATED ROLE ROLE_ADMIN</>');
		} else {
			$output->writeln('<bg=red;options=bold>ROLE ROLE_ADMIN ALREADY EXISTS</>');
		}

		$this->em->flush();
	}

	private function users(OutputInterface $output): void
	{
		$superAdminExists = $this->em->getRepository(User::class)->findOneBy([
			'username' => "superadmin@superadmin.com"
		]);
		if (!$superAdminExists) {
			$roleSuperAdminExists = $this->em->getRepository(Role::class)->findOneBy([
				'name' => "ROLE_SUPERADMIN"
			]);
			$superAdmin = new User();
			$superAdmin->setUsername('superadmin@superadmin.com');
			$superAdmin->setEmail('superadmin@superadmin.com');
			$superAdmin->setRole($roleSuperAdminExists);
			$superAdmin->setPassword($this->passwordHasher->hashPassword($superAdmin, 'superadmin'));
			$this->em->persist($superAdmin);
			$output->writeln('<bg=green;options=bold>CREATED USER superadmin@superadmin.com</>');
		} else {
			$output->writeln('<bg=red;options=bold>USER superadmin@superadmin.com ALREADY EXISTS</>');
		}

		$adminExists = $this->em->getRepository(User::class)->findOneBy([
			'username' => "admin@admin.com"
		]);
		if (!$adminExists) {
			$roleAdminExists = $this->em->getRepository(Role::class)->findOneBy([
				'name' => "ROLE_ADMIN"
			]);
			$admin = new User();
			$admin->setUsername('admin@admin.com');
			$admin->setEmail('admin@admin.com');
			$admin->setRole($roleAdminExists);
			$admin->setPassword($this->passwordHasher->hashPassword($admin, 'admin'));
			$this->em->persist($admin);
			$output->writeln('<bg=green;options=bold>CREATED USER admin@admin.com</>');
		} else {
			$output->writeln('<bg=red;options=bold>USER admin@admin.com ALREADY EXISTS</>');
		}

		$this->em->flush();
	}
}
