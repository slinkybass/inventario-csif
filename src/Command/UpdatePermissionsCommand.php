<?php

namespace App\Command;

use App\Entity\Role;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManagerInterface;

class UpdatePermissionsCommand extends Command
{
	private $em;

	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;

		parent::__construct();
	}

	protected static $defaultName = 'app:update-permissions';

	protected function configure(): void
	{
		$this->setHelp("This command allows you to set all permissions to ROLE_SUPERADMIN.");
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$this->permissions($output);

		return Command::SUCCESS;
	}

	private function permissions(OutputInterface $output): void
	{
		$roleSuperAdminExists = $this->em->getRepository(Role::class)->findOneBy([
			'name' => "ROLE_SUPERADMIN"
		]);
		if ($roleSuperAdminExists) {
			$permissions = array();
			foreach (Role::PERMISSIONS as $k => $v) {
				$permissions[$k] = true;
			}
			$roleSuperAdminExists->setPermissions($permissions);
			$this->em->persist($roleSuperAdminExists);
			$output->writeln('<bg=green;options=bold>UPDATED ROLE ROLE_SUPERADMIN</>');
		}

		$this->em->flush();
	}
}
