<?php

namespace App\EventSubscriber;

use App\Entity\Config;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

use Doctrine\ORM\EntityManagerInterface;

class ConfigSuscriber implements EventSubscriberInterface
{
	private $em;
	private $config;

	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->em = $entityManager;

		//Definition of default config
		$this->config = new \stdClass();
		$this->config->appName = 'Symfony 6 Base';
		$this->config->appLogo = '/build/images/logo.png';
		$this->config->appFavicon = '/build/images/favicon.png';
		$this->config->locale = 'es';
		$this->config->enableUsername = false;
		$this->config->enablePublic = false;
	}

	public function onKernelRequest(RequestEvent $event)
	{
		$request = $event->getRequest();

		//Set variables
		$dcConfig = $this->em->getRepository(Config::class)->findOneBy(array(), array('id' => 'DESC'));
		if ($dcConfig) {
			$this->config->appName = $dcConfig->getAppName() ?? $this->config->appName;
			$this->config->appLogo = $dcConfig->getAppLogo() ?? $this->config->appLogo;
			$this->config->appFavicon = $dcConfig->getAppFavicon() ?? $this->config->appFavicon;
			$this->config->locale = $dcConfig->getLocale() ?? $this->config->locale;
			$this->config->enableUsername = $dcConfig->getEnableUsername() ?? $this->config->enableUsername;
			$this->config->enablePublic = $dcConfig->getEnablePublic() ?? $this->config->enablePublic;
		}

		//Set locale
		$request->setLocale($this->config->locale);

		//Return config variable
		$event->getRequest()->getSession()->set('config', $this->config);
	}

	public static function getSubscribedEvents()
	{
		return [
			KernelEvents::REQUEST => [['onKernelRequest', 20]],
		];
	}
}
