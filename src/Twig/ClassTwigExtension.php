<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ClassTwigExtension extends AbstractExtension
{
	public function getFunctions()
	{
		return [
			'class' => new TwigFunction('class', array($this, 'getClass'))
		];
	}

	public function getName()
	{
		return 'class_twig_extension';
	}

	public function getClass($object)
	{
		return (new \ReflectionClass($object))->getShortName();
	}
}
