<?php

namespace App\Entity;

use App\Repository\PersonalRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonalRepository::class)
 * @ORM\Table(name="personal")
 */
class Personal
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $lastname;

	/**
	 * @ORM\ManyToMany(targetEntity=Puesto::class, inversedBy="personal")
	 */
	private $puestos;

	public function __construct()
	{
		$this->puestos = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getFullname();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getLastname(): ?string
	{
		return $this->lastname;
	}

	public function setLastname(?string $lastname): self
	{
		$this->lastname = $lastname;

		return $this;
	}

	public function getFullname(): ?string
	{
		$str = $this->getName();
		$str .= $this->getLastname() ? ' ' . $this->getLastname() : null;
		return $str;
	}

	/**
	 * @return Collection|Puesto[]
	 */
	public function getPuestos(): Collection
	{
		return $this->puestos;
	}

	public function addPuesto(Puesto $puesto): self
	{
		if (!$this->puestos->contains($puesto)) {
			$this->puestos[] = $puesto;
		}

		return $this;
	}

	public function removePuesto(Puesto $puesto): self
	{
		$this->puestos->removeElement($puesto);

		return $this;
	}
}
