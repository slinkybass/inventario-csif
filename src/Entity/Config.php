<?php

namespace App\Entity;

use App\Repository\ConfigRepository;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConfigRepository::class)
 * @ORM\Table(name="config")
 */
class Config
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $appName;

	/**
	 * @ORM\Column(type="string", length=6, nullable=true)
	 */
	private $locale;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $enableUsername;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $enablePublic;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $appLogo;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $appFavicon;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getAppName(): ?string
	{
		return $this->appName;
	}

	public function setAppName(?string $appName): self
	{
		$this->appName = $appName;

		return $this;
	}

	public function getLocale(): ?string
	{
		return $this->locale;
	}

	public function setLocale(?string $locale): self
	{
		$this->locale = $locale;

		return $this;
	}

	public function getEnableUsername(): ?bool
	{
		return $this->enableUsername;
	}

	public function setEnableUsername(bool $enableUsername): self
	{
		$this->enableUsername = $enableUsername;

		return $this;
	}

	public function getEnablePublic(): ?bool
	{
		return $this->enablePublic;
	}

	public function setEnablePublic(bool $enablePublic): self
	{
		$this->enablePublic = $enablePublic;

		return $this;
	}

	public function getAppLogo(): ?string
	{
		return $this->appLogo;
	}

	public function setAppLogo(?string $appLogo): self
	{
		$this->appLogo = $appLogo;

		return $this;
	}

	public function getAppFavicon(): ?string
	{
		return $this->appFavicon;
	}

	public function setAppFavicon(?string $appFavicon): self
	{
		$this->appFavicon = $appFavicon;

		return $this;
	}
}
