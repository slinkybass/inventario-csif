<?php

namespace App\Entity;

use App\Repository\PlantaRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlantaRepository::class)
 * @ORM\Table(name="planta")
 */
class Planta
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $orden;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\ManyToOne(targetEntity=Sede::class, inversedBy="plantas")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $sede;

	/**
	 * @ORM\OneToMany(targetEntity=Despacho::class, mappedBy="planta", orphanRemoval=true)
	 * @ORM\OrderBy({"orden"="ASC"})
	 */
	private $despachos;

	/**
	 * @ORM\OneToMany(targetEntity=ElementoInventario::class, mappedBy="planta")
	 */
	private $elementosInventario;

	public function __construct()
	{
		$this->despachos = new ArrayCollection();
		$this->elementosInventario = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getSede() . ' - ' . $this->getName();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getOrden(): ?int
	{
		return $this->orden;
	}

	public function setOrden(int $orden): self
	{
		$this->orden = $orden;

		return $this;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getSede(): ?Sede
	{
		return $this->sede;
	}

	public function setSede(?Sede $sede): self
	{
		$this->sede = $sede;

		return $this;
	}

	/**
	 * @return Collection|Despacho[]
	 */
	public function getDespachos(): Collection
	{
		return $this->despachos;
	}

	public function addDespacho(Despacho $despacho): self
	{
		if (!$this->despachos->contains($despacho)) {
			$this->despachos[] = $despacho;
			$despacho->setPlanta($this);
		}

		return $this;
	}

	public function removeDespacho(Despacho $despacho): self
	{
		if ($this->despachos->removeElement($despacho)) {
			// set the owning side to null (unless already changed)
			if ($despacho->getPlanta() === $this) {
				$despacho->setPlanta(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|ElementoInventario[]
	 */
	public function getElementosInventario(): Collection
	{
		return $this->elementosInventario;
	}

	public function addElementosInventario(ElementoInventario $elementoInventario): self
	{
		if (!$this->elementosInventario->contains($elementoInventario)) {
			$this->elementosInventario[] = $elementoInventario;
			$elementoInventario->setPlanta($this);
		}

		return $this;
	}

	public function removeElementosInventario(ElementoInventario $elementoInventario): self
	{
		if ($this->elementosInventario->removeElement($elementoInventario)) {
			// set the owning side to null (unless already changed)
			if ($elementoInventario->getPlanta() === $this) {
				$elementoInventario->setPlanta(null);
			}
		}

		return $this;
	}

	public function setElementosInventario($elementosInventario): self
	{
		foreach ($this->elementosInventario as $elementoInventario) {
			$this->removeElementosInventario($elementoInventario);
		}
		foreach ($elementosInventario as $elementoInventario) {
			$this->addElementosInventario($elementoInventario);
		}

		return $this;
	}
}
