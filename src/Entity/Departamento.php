<?php

namespace App\Entity;

use App\Repository\DepartamentoRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DepartamentoRepository::class)
 * @ORM\Table(name="departamento")
 */
class Departamento
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\OneToMany(targetEntity=Despacho::class, mappedBy="departamento")
	 */
	private $despachos;

	public function __construct()
	{
		$this->despachos = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getName();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return Collection|Despacho[]
	 */
	public function getDespachos(): Collection
	{
		return $this->despachos;
	}

	public function addDespacho(Despacho $despacho): self
	{
		if (!$this->despachos->contains($despacho)) {
			$this->despachos[] = $despacho;
			$despacho->setDepartamento($this);
		}

		return $this;
	}

	public function removeDespacho(Despacho $despacho): self
	{
		if ($this->despachos->removeElement($despacho)) {
			// set the owning side to null (unless already changed)
			if ($despacho->getDepartamento() === $this) {
				$despacho->setDepartamento(null);
			}
		}

		return $this;
	}
}
