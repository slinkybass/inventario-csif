<?php

namespace App\Entity;

use App\Repository\LicenseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LicenseRepository::class)
 */
class License
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $serial;

	/**
	 * @ORM\ManyToOne(targetEntity=ElementoInventario::class, inversedBy="licenses")
	 */
	private $elementoInventario;

	public function __toString(): string
	{
		$name = $this->getName();
		$serial = $this->getSerial() ? '[' . $this->getSerial() . ']' : null;
		return implode(" ", array_filter([$name, $serial]));
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getSerial(): ?string
	{
		return $this->serial;
	}

	public function setSerial(?string $serial): self
	{
		$this->serial = $serial;

		return $this;
	}

	public function getElementoInventario(): ?ElementoInventario
	{
		return $this->elementoInventario;
	}

	public function setElementoInventario(?ElementoInventario $elementoInventario): self
	{
		$this->elementoInventario = $elementoInventario;

		return $this;
	}
}
