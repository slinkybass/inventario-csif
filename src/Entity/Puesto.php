<?php

namespace App\Entity;

use App\Repository\PuestoRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PuestoRepository::class)
 * @ORM\Table(name="puesto")
 */
class Puesto
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $orden;

	/**
	 * @ORM\ManyToOne(targetEntity=Despacho::class, inversedBy="puestos")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $despacho;

	/**
	 * @ORM\ManyToMany(targetEntity=Personal::class, mappedBy="puestos")
	 */
	private $personal;

	/**
	 * @ORM\OneToMany(targetEntity=ElementoInventario::class, mappedBy="puesto")
	 */
	private $elementosInventario;

	public function __construct()
	{
		$this->personal = new ArrayCollection();
		$this->elementosInventario = new ArrayCollection();
	}

	public function __toString(): string
	{
		$str = $this->getDespacho() . ' - Nº ' . $this->getOrden();
		if (count($this->getPersonal())) {
			$str .= ' (';
			$i = 1;
			foreach ($this->getPersonal() as $personal) {
				$str .= $personal;
				if ($i != count($this->getPersonal())) {
					$str .= ', ';
				}
				$i++;
			}
			$str .= ')';
		}
		return $str;
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getOrden(): ?int
	{
		return $this->orden;
	}

	public function setOrden(int $orden): self
	{
		$this->orden = $orden;

		return $this;
	}

	public function getDespacho(): ?Despacho
	{
		return $this->despacho;
	}

	public function setDespacho(?Despacho $despacho): self
	{
		$this->despacho = $despacho;

		return $this;
	}

	/**
	 * @return Collection|Personal[]
	 */
	public function getPersonal(): Collection
	{
		return $this->personal;
	}

	public function addPersonal(Personal $personal): self
	{
		if (!$this->personal->contains($personal)) {
			$this->personal[] = $personal;
			$personal->addPuesto($this);
		}

		return $this;
	}

	public function removePersonal(Personal $personal): self
	{
		if ($this->personal->removeElement($personal)) {
			$personal->removePuesto($this);
		}

		return $this;
	}

	/**
	 * @return Collection|ElementoInventario[]
	 */
	public function getElementosInventario(): Collection
	{
		return $this->elementosInventario;
	}

	public function addElementosInventario(ElementoInventario $elementoInventario): self
	{
		if (!$this->elementosInventario->contains($elementoInventario)) {
			$this->elementosInventario[] = $elementoInventario;
			$elementoInventario->setPuesto($this);
		}

		return $this;
	}

	public function removeElementosInventario(ElementoInventario $elementoInventario): self
	{
		if ($this->elementosInventario->removeElement($elementoInventario)) {
			// set the owning side to null (unless already changed)
			if ($elementoInventario->getPuesto() === $this) {
				$elementoInventario->setPuesto(null);
			}
		}

		return $this;
	}

	public function setElementosInventario($elementosInventario): self
	{
		foreach ($this->elementosInventario as $elementoInventario) {
			$this->removeElementosInventario($elementoInventario);
		}
		foreach ($elementosInventario as $elementoInventario) {
			$this->addElementosInventario($elementoInventario);
		}

		return $this;
	}
}
