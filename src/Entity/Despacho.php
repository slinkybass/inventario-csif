<?php

namespace App\Entity;

use App\Repository\DespachoRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DespachoRepository::class)
 * @ORM\Table(name="despacho")
 */
class Despacho
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $orden;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\ManyToOne(targetEntity=Planta::class, inversedBy="despachos")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $planta;

	/**
	 * @ORM\OneToMany(targetEntity=Puesto::class, mappedBy="despacho", orphanRemoval=true)
	 * @ORM\OrderBy({"orden"="ASC"})
	 */
	private $puestos;

	/**
	 * @ORM\ManyToOne(targetEntity=Departamento::class, inversedBy="despachos")
	 */
	private $departamento;

	/**
	 * @ORM\OneToMany(targetEntity=ElementoInventario::class, mappedBy="despacho")
	 */
	private $elementosInventario;

	public function __construct()
	{
		$this->puestos = new ArrayCollection();
		$this->elementosInventario = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getPlanta()->getSede() . ' - ' . $this->getName();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getOrden(): ?int
	{
		return $this->orden;
	}

	public function setOrden(int $orden): self
	{
		$this->orden = $orden;

		return $this;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getPlanta(): ?Planta
	{
		return $this->planta;
	}

	public function setPlanta(?Planta $planta): self
	{
		$this->planta = $planta;

		return $this;
	}

	/**
	 * @return Collection|Puesto[]
	 */
	public function getPuestos(): Collection
	{
		return $this->puestos;
	}

	public function addPuesto(Puesto $puesto): self
	{
		if (!$this->puestos->contains($puesto)) {
			$this->puestos[] = $puesto;
			$puesto->setDespacho($this);
		}

		return $this;
	}

	public function removePuesto(Puesto $puesto): self
	{
		if ($this->puestos->removeElement($puesto)) {
			// set the owning side to null (unless already changed)
			if ($puesto->getDespacho() === $this) {
				$puesto->setDespacho(null);
			}
		}

		return $this;
	}

	public function getDepartamento(): ?Departamento
	{
		return $this->departamento;
	}

	public function setDepartamento(?Departamento $departamento): self
	{
		$this->departamento = $departamento;

		return $this;
	}

	/**
	 * @return Collection|ElementoInventario[]
	 */
	public function getElementosInventario(): Collection
	{
		return $this->elementosInventario;
	}

	public function addElementosInventario(ElementoInventario $elementoInventario): self
	{
		if (!$this->elementosInventario->contains($elementoInventario)) {
			$this->elementosInventario[] = $elementoInventario;
			$elementoInventario->setDespacho($this);
		}

		return $this;
	}

	public function removeElementosInventario(ElementoInventario $elementoInventario): self
	{
		if ($this->elementosInventario->removeElement($elementoInventario)) {
			// set the owning side to null (unless already changed)
			if ($elementoInventario->getDespacho() === $this) {
				$elementoInventario->setDespacho(null);
			}
		}

		return $this;
	}

	public function setElementosInventario($elementosInventario): self
	{
		foreach ($this->elementosInventario as $elementoInventario) {
			$this->removeElementosInventario($elementoInventario);
		}
		foreach ($elementosInventario as $elementoInventario) {
			$this->addElementosInventario($elementoInventario);
		}

		return $this;
	}
}
