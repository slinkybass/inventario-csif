<?php

namespace App\Entity;

use App\Repository\ElementoInventarioRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ElementoInventarioRepository::class)
 * @ORM\Table(name="elementoInventario")
 */
class ElementoInventario
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $identificator;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $name;

	/**
	 * @ORM\ManyToOne(targetEntity=TipoInventario::class, inversedBy="elementosInventario")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $tipoInventario;

	/**
	 * @ORM\ManyToOne(targetEntity=Puesto::class, inversedBy="elementosInventario")
	 */
	private $puesto;

	/**
	 * @ORM\ManyToOne(targetEntity=Despacho::class, inversedBy="elementosInventario")
	 */
	private $despacho;

	/**
	 * @ORM\ManyToOne(targetEntity=Planta::class, inversedBy="elementosInventario")
	 */
	private $planta;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $extension;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $remote;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $ip;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $gateway;

	/**
	 * @ORM\OneToMany(targetEntity=License::class, mappedBy="elementoInventario")
	 */
	private $licenses;

	public function __construct()
	{
		$this->licenses = new ArrayCollection();
	}

	public function __toString(): string
	{
		$tipoInventario = $this->getTipoInventario();
		$name = $this->getName();
		$id = $this->getIdentificator() ? '[' . $this->getIdentificator() . ']' : null;
		$site = $this->getSite() ? $this->getSite() : null;
		return implode(" - ", array_filter([$tipoInventario, $name, $id, $site]));
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getIdentificator(): ?string
	{
		return $this->identificator;
	}

	public function setIdentificator(?string $identificator): self
	{
		$this->identificator = $identificator;

		return $this;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(?string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getTipoInventario(): ?TipoInventario
	{
		return $this->tipoInventario;
	}

	public function setTipoInventario(?TipoInventario $tipoInventario): self
	{
		$this->tipoInventario = $tipoInventario;

		return $this;
	}

	public function getPuesto(): ?Puesto
	{
		return $this->puesto;
	}

	public function setPuesto(?Puesto $puesto): self
	{
		$this->puesto = $puesto;

		return $this;
	}

	public function getDespacho(): ?Despacho
	{
		return $this->despacho;
	}

	public function setDespacho(?Despacho $despacho): self
	{
		$this->despacho = $despacho;

		return $this;
	}

	public function getPlanta(): ?Planta
	{
		return $this->planta;
	}

	public function setPlanta(?Planta $planta): self
	{
		$this->planta = $planta;

		return $this;
	}

	public function getSite(): ?string
	{
		return $this->getPuesto() ?? $this->getDespacho() ?? $this->getPlanta();
	}

	public function getExtension(): ?string
	{
		return $this->extension;
	}

	public function setExtension(?string $extension): self
	{
		$this->extension = $extension;

		return $this;
	}

	public function getRemote(): ?bool
	{
		return $this->remote;
	}

	public function setRemote(?bool $remote): self
	{
		$this->remote = $remote;

		return $this;
	}

	public function getIp(): ?string
	{
		return $this->ip;
	}

	public function setIp(?string $ip): self
	{
		$this->ip = $ip;

		return $this;
	}

	public function getGateway(): ?string
	{
		return $this->gateway;
	}

	public function setGateway(?string $gateway): self
	{
		$this->gateway = $gateway;

		return $this;
	}

	public function getExtraInfo(): ?string
	{
		$info[] = $this->extension ? $this->extension : null;
		$info[] = $this->ip ? $this->ip : null;
		$info[] = $this->gateway ? $this->gateway : null;
		$info[] = $this->remote ? '[REMOTE]' : null;
		return implode('<br />', array_filter($info));
	}

	/**
	 * @return Collection<int, License>
	 */
	public function getLicenses(): Collection
	{
		return $this->licenses;
	}

	public function addLicense(License $license): self
	{
		if (!$this->licenses->contains($license)) {
			$this->licenses[] = $license;
			$license->setElementoInventario($this);
		}

		return $this;
	}

	public function removeLicense(License $license): self
	{
		if ($this->licenses->removeElement($license)) {
			// set the owning side to null (unless already changed)
			if ($license->getElementoInventario() === $this) {
				$license->setElementoInventario(null);
			}
		}

		return $this;
	}
}
