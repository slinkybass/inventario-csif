<?php

namespace App\Entity;

use App\Repository\TipoInventarioRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TipoInventarioRepository::class)
 * @ORM\Table(name="tipoInventario")
 */
class TipoInventario
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $orden;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $elementosPuesto;

	/**
	 * @ORM\ManyToOne(targetEntity=CategoriaInventario::class, inversedBy="tiposInventario")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $categoriaInventario;

	/**
	 * @ORM\OneToMany(targetEntity=ElementoInventario::class, mappedBy="tipoInventario", orphanRemoval=true)
	 */
	private $elementosInventario;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $hasExtension;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $hasRemote;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $hasIP;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $hasGateway;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $hasLicenses;

	public function __construct()
	{
		$this->elementosInventario = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getName();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getOrden(): ?int
	{
		return $this->orden;
	}

	public function setOrden(int $orden): self
	{
		$this->orden = $orden;

		return $this;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getElementosPuesto(): ?bool
	{
		return $this->elementosPuesto;
	}

	public function setElementosPuesto(bool $elementosPuesto): self
	{
		$this->elementosPuesto = $elementosPuesto;

		return $this;
	}

	public function getCategoriaInventario(): ?CategoriaInventario
	{
		return $this->categoriaInventario;
	}

	public function setCategoriaInventario(?CategoriaInventario $categoriaInventario): self
	{
		$this->categoriaInventario = $categoriaInventario;

		return $this;
	}

	/**
	 * @return Collection|ElementoInventario[]
	 */
	public function getElementosInventario(): Collection
	{
		return $this->elementosInventario;
	}

	public function addElementoInventario(ElementoInventario $elementoInventario): self
	{
		if (!$this->elementosInventario->contains($elementoInventario)) {
			$this->elementosInventario[] = $elementoInventario;
			$elementoInventario->setTipoInventario($this);
		}

		return $this;
	}

	public function removeElementoInventario(ElementoInventario $elementoInventario): self
	{
		if ($this->elementosInventario->removeElement($elementoInventario)) {
			// set the owning side to null (unless already changed)
			if ($elementoInventario->getTipoInventario() === $this) {
				$elementoInventario->setTipoInventario(null);
			}
		}

		return $this;
	}

	public function getHasExtension(): ?bool
	{
		return $this->hasExtension;
	}

	public function setHasExtension(bool $hasExtension): self
	{
		$this->hasExtension = $hasExtension;

		return $this;
	}

	public function getHasRemote(): ?bool
	{
		return $this->hasRemote;
	}

	public function setHasRemote(bool $hasRemote): self
	{
		$this->hasRemote = $hasRemote;

		return $this;
	}

	public function getHasIP(): ?bool
	{
		return $this->hasIP;
	}

	public function setHasIP(bool $hasIP): self
	{
		$this->hasIP = $hasIP;

		return $this;
	}

	public function getHasGateway(): ?bool
	{
		return $this->hasGateway;
	}

	public function setHasGateway(bool $hasGateway): self
	{
		$this->hasGateway = $hasGateway;

		return $this;
	}

	public function getHasLicenses(): ?bool
	{
		return $this->hasLicenses;
	}

	public function setHasLicenses(bool $hasLicenses): self
	{
		$this->hasLicenses = $hasLicenses;

		return $this;
	}
}
