<?php

namespace App\Entity;

use App\Repository\SedeRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SedeRepository::class)
 * @ORM\Table(name="sede")
 */
class Sede
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $orden;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $address;

	/**
	 * @ORM\OneToMany(targetEntity=Planta::class, mappedBy="sede", orphanRemoval=true)
	 * @ORM\OrderBy({"orden"="ASC"})
	 */
	private $plantas;

	public function __construct()
	{
		$this->plantas = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getName();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getOrden(): ?int
	{
		return $this->orden;
	}

	public function setOrden(int $orden): self
	{
		$this->orden = $orden;

		return $this;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getAddress(): ?string
	{
		return $this->address;
	}

	public function setAddress(?string $address): self
	{
		$this->address = $address;

		return $this;
	}

	/**
	 * @return Collection|Planta[]
	 */
	public function getPlantas(): Collection
	{
		return $this->plantas;
	}

	public function addPlanta(Planta $planta): self
	{
		if (!$this->plantas->contains($planta)) {
			$this->plantas[] = $planta;
			$planta->setSede($this);
		}

		return $this;
	}

	public function removePlanta(Planta $planta): self
	{
		if ($this->plantas->removeElement($planta)) {
			// set the owning side to null (unless already changed)
			if ($planta->getSede() === $this) {
				$planta->setSede(null);
			}
		}

		return $this;
	}
}
