<?php

namespace App\Entity;

use App\Repository\CategoriaInventarioRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoriaInventarioRepository::class)
 * @ORM\Table(name="categoriaInventario")
 */
class CategoriaInventario
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\OneToMany(targetEntity=TipoInventario::class, mappedBy="categoriaInventario", orphanRemoval=true)
	 */
	private $tiposInventario;

	public function __construct()
	{
		$this->tiposInventario = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getName();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return Collection|TipoInventario[]
	 */
	public function getTiposInventario(): Collection
	{
		return $this->tiposInventario;
	}

	public function addTipoInventario(TipoInventario $tipoInventario): self
	{
		if (!$this->tiposInventario->contains($tipoInventario)) {
			$this->tiposInventario[] = $tipoInventario;
			$tipoInventario->setCategoriaInventario($this);
		}

		return $this;
	}

	public function removeTipoInventario(TipoInventario $tipoInventario): self
	{
		if ($this->tiposInventario->removeElement($tipoInventario)) {
			// set the owning side to null (unless already changed)
			if ($tipoInventario->getCategoriaInventario() === $this) {
				$tipoInventario->setCategoriaInventario(null);
			}
		}

		return $this;
	}
}
