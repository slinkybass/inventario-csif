// Mark.js
import Mark from "mark.js/src/vanilla";

// DirtyForm
import DirtyForm from "dirty-form";

// TomSelect
import Autocomplete from "./custom-js/autocomplete.js";

// ON LOAD
document.addEventListener("DOMContentLoaded", function () {
	Admin.createSearchHighlight();
	Admin.createFilters();
	Admin.createToggleFields();
	Admin.createBatchActions();
	Admin.createModalWindowsForDeleteActions();
	Admin.createAsyncFields();
	Admin.createUnsavedFormChangesWarning();
	Admin.preventMultipleFormSubmission();

	document.addEventListener("ea.collection.item-added", function () {
		Admin.createAsyncFields();
	});
});

const Admin = (() => {
	const createSearchHighlight = () => {
		var searchElement = document.querySelector(
			'.form-action-search [name="query"]'
		);
		if (searchElement) {
			var searchQuery = searchElement.value;
			if (searchQuery.trim()) {
				var elementsToHighlight = document.querySelectorAll(
					"table tbody td:not(.actions)"
				);
				var highlighter = new Mark(elementsToHighlight);
				highlighter.mark(searchQuery);
			}
		}
	};

	const createFilters = () => {
		var filterButton = document.querySelector(
			".datagrid-filters .action-filters-button"
		);
		if (null === filterButton) {
			return;
		}

		var filterModal = document.querySelector(
			filterButton.getAttribute("data-bs-target")
		);

		// this is needed to avoid errors when connection is slow
		filterButton.setAttribute("href", filterButton.getAttribute("data-href"));
		filterButton.removeAttribute("data-href");
		filterButton.classList.remove("disabled");

		filterButton.addEventListener("click", (event) => {
			var filterModalBody = filterModal.querySelector(".modal-body");
			filterModalBody.innerHTML =
				'<div class="fa-3x px-3 py-3 text-muted text-center"><i class="fas fa-circle-notch fa-spin"></i></div>';

			fetch(filterButton.getAttribute("href"))
				.then((response) => {
					return response.text();
				})
				.then((text) => {
					setInnerHTMLAndRunScripts(filterModalBody, text);
					Admin.createAsyncFields();
					Admin.createFilterToggles();
				})
				.catch((error) => {
					console.error(error);
				});

			event.preventDefault();
		});

		const removeFilter = (filterField) => {
			filterField
				.closest("form")
				.querySelectorAll(
					`input[name^="filters[${filterField.dataset.filterProperty}]"]`
				)
				.forEach((filterFieldInput) => {
					filterFieldInput.remove();
				});

			filterField.remove();
		};

		document
			.querySelector("#modal-clear-button")
			.addEventListener("click", () => {
				filterModal.querySelectorAll(".form-check").forEach((filterField) => {
					removeFilter(filterField);
				});
				filterModal.querySelector("form").submit();
			});

		document
			.querySelector("#modal-apply-button")
			.addEventListener("click", () => {
				filterModal
					.querySelectorAll(".filter-checkbox:not(:checked)")
					.forEach((notAppliedFilter) => {
						removeFilter(notAppliedFilter.closest(".form-check"));
					});
				filterModal.querySelector("form").submit();
			});
	};

	const createToggleFields = () => {
		const disableToggleField = (toggleField, isChecked) => {
			toggleField.checked = isChecked;
			toggleField.disabled = true;
			toggleField.closest(".custom-switch").classList.add("disabled");
		};
		document
			.querySelectorAll('td.field-boolean .form-switch input[type="checkbox"]')
			.forEach((toggleField) => {
				toggleField.addEventListener("change", () => {
					var newValue = toggleField.checked;
					var oldValue = !newValue;
					var toggleUrl =
						toggleField.getAttribute("data-toggle-url") +
						"&newValue=" +
						newValue.toString();
					fetch(toggleUrl, {
						headers: { "X-Requested-With": "XMLHttpRequest" },
					})
						.then((response) => {
							if (!response.ok) {
								disableToggleField(toggleField, oldValue);
							}
							return response.text();
						})
						.catch(() => disableToggleField(toggleField, oldValue));
				});
			});
	};

	const createBatchActions = () => {
		var selectAllCheckbox = document.querySelector(".form-batch-checkbox-all");
		if (selectAllCheckbox) {
			var rowCheckboxes = document.querySelectorAll(
				'input[type="checkbox"].form-batch-checkbox'
			);
			selectAllCheckbox.addEventListener("change", () => {
				rowCheckboxes.forEach((rowCheckbox) => {
					rowCheckbox.checked = selectAllCheckbox.checked;
					rowCheckbox.dispatchEvent(new Event("change"));
				});
			});
			rowCheckboxes.forEach((rowCheckbox) => {
				rowCheckbox.addEventListener("change", () => {
					var selectedRowCheckboxes = document.querySelectorAll(
						'input[type="checkbox"].form-batch-checkbox:checked'
					);
					if (!rowCheckbox.checked) {
						selectAllCheckbox.checked = false;
					}
					var rowsAreSelected = 0 !== selectedRowCheckboxes.length;
					var contentTitle = document.querySelector(
						".content-header-title > .title"
					);
					var filters = document.querySelector(".datagrid-filters");
					var globalActions = document.querySelector(".global-actions");
					var batchActions = document.querySelector(".batch-actions");
					if (contentTitle) {
						contentTitle.style.visibility = rowsAreSelected
							? "hidden"
							: "visible";
					}
					if (filters) {
						filters.style.display = rowsAreSelected ? "none" : "block";
					}
					if (globalActions) {
						globalActions.style.display = rowsAreSelected ? "none" : "block";
					}
					if (batchActions) {
						batchActions.style.display = rowsAreSelected ? "block" : "none";
					}
				});
			});
			var modalTitle = document.querySelector(
				"#batch-action-confirmation-title"
			);
			var titleContentWithPlaceholders = modalTitle.textContent;
			document
				.querySelectorAll("[data-action-batch]")
				.forEach((dataActionBatch) => {
					dataActionBatch.addEventListener("click", (event) => {
						event.preventDefault();
						var actionElement =
							event.target.tagName.toUpperCase() === "A"
								? event.target
								: event.target.parentNode;
						var actionName =
							actionElement.textContent.trim() ||
							actionElement.getAttribute("title");
						var selectedItems = document.querySelectorAll(
							'input[type="checkbox"].form-batch-checkbox:checked'
						);
						modalTitle.textContent = titleContentWithPlaceholders
							.replace("%action_name%", actionName)
							.replace("%num_items%", selectedItems.length.toString());
						document
							.querySelector("#modal-batch-action-button")
							.addEventListener("click", () => {
								actionElement.setAttribute("disabled", "disabled");
								var batchFormFields = {
									batchActionName:
										actionElement.getAttribute("data-action-name"),
									entityFqcn: actionElement.getAttribute("data-entity-fqcn"),
									batchActionUrl: actionElement.getAttribute("data-action-url"),
									batchActionCsrfToken: actionElement.getAttribute(
										"data-action-csrf-token"
									),
								};
								selectedItems.forEach((item, i) => {
									batchFormFields[`batchActionEntityIds[${i}]`] = item.value;
								});
								var batchForm = document.createElement("form");
								batchForm.setAttribute("method", "POST");
								batchForm.setAttribute(
									"action",
									actionElement.getAttribute("data-action-url")
								);
								for (let fieldName in batchFormFields) {
									var formField = document.createElement("input");
									formField.setAttribute("type", "hidden");
									formField.setAttribute("name", fieldName);
									formField.setAttribute("value", batchFormFields[fieldName]);
									batchForm.appendChild(formField);
								}
								document.body.appendChild(batchForm);
								batchForm.submit();
							});
					});
				});
		}
	};

	const createModalWindowsForDeleteActions = () => {
		document.querySelectorAll(".action-delete").forEach((actionElement) => {
			actionElement.addEventListener("click", (event) => {
				event.preventDefault();
				document
					.querySelector("#modal-delete-button")
					.addEventListener("click", () => {
						var deleteFormAction = actionElement.getAttribute("formaction");
						var deleteForm = document.querySelector("#delete-form");
						deleteForm.setAttribute("action", deleteFormAction);
						deleteForm.submit();
					});
			});
		});
	};

	const createAsyncFields = () => {
		const autocomplete = new Autocomplete();
		document
			.querySelectorAll('[data-ea-widget="ea-autocomplete"]')
			.forEach((autocompleteElement) => {
				autocomplete.create(autocompleteElement);
			});
	};

	const createFilterToggles = () => {
		document.querySelectorAll(".filter-checkbox").forEach((filterCheckbox) => {
			filterCheckbox.addEventListener("change", () => {
				const filterToggleLink = filterCheckbox.nextElementSibling;
				const filterExpandedAttribute =
					filterCheckbox.nextElementSibling.getAttribute("aria-expanded");

				if (
					(filterCheckbox.checked && "false" === filterExpandedAttribute) ||
					(!filterCheckbox.checked && "true" === filterExpandedAttribute)
				) {
					filterToggleLink.click();
				}
			});
		});

		document
			.querySelectorAll("form[data-ea-filters-form-id]")
			.forEach((form) => {
				// TODO: when using the native datepicker, 'change' isn't fired unless you input the entire date + time information
				form.addEventListener("change", (event) => {
					if (event.target.classList.contains("filter-checkbox")) {
						return;
					}

					const filterCheckbox = event.target
						.closest(".filter-field")
						.querySelector(".filter-checkbox");
					if (!filterCheckbox.checked) {
						filterCheckbox.checked = true;
					}
				});
			});

		document
			.querySelectorAll("[data-ea-comparison-id]")
			.forEach((comparisonWidget) => {
				comparisonWidget.addEventListener("change", (event) => {
					const comparisonWidget = event.currentTarget;
					const comparisonId = comparisonWidget.dataset.eaComparisonId;

					if (comparisonId === undefined) {
						return;
					}

					const secondValue = document.querySelector(
						`[data-ea-value2-of-comparison-id="${comparisonId}"]`
					);

					if (secondValue === null) {
						return;
					}

					secondValue.style.display =
						comparisonWidget.value === "between" ? "" : "none";
				});
			});
	};

	const createUnsavedFormChangesWarning = () => {
		[".ea-new-form", ".ea-edit-form"].forEach((formSelector) => {
			var form = document.querySelector(formSelector);
			if (form) {
				new DirtyForm(form);
			}
		});
	};

	const preventMultipleFormSubmission = () => {
		[".ea-new-form", ".ea-edit-form"].forEach((formSelector) => {
			var form = document.querySelector(formSelector);
			if (form) {
				form.addEventListener(
					"submit",
					() => {
						setTimeout(() => {
							var submitButtons = document
								.querySelector(".ea-edit, .ea-new")
								.querySelectorAll('[type="submit"]');
							submitButtons.forEach((button) => {
								button.classList.add("btn-loading");
								button.setAttribute("disabled", "disabled");
							});
						}, 1);
					},
					false
				);
			}
		});
	};

	const setInnerHTMLAndRunScripts = (element, htmlContent) => {
		element.innerHTML = htmlContent;
		Array.from(element.querySelectorAll("script")).forEach((oldScript) => {
			var newScript = document.createElement("script");
			Array.from(oldScript.attributes).forEach((attr) =>
				newScript.setAttribute(attr.name, attr.value)
			);
			newScript.appendChild(document.createTextNode(oldScript.innerHTML));
			oldScript.parentNode.replaceChild(newScript, oldScript);
		});
	};

	return {
		createSearchHighlight: createSearchHighlight,
		createFilters: createFilters,
		createToggleFields: createToggleFields,
		createBatchActions: createBatchActions,
		createModalWindowsForDeleteActions: createModalWindowsForDeleteActions,
		createAsyncFields: createAsyncFields,
		createFilterToggles: createFilterToggles,
		createUnsavedFormChangesWarning: createUnsavedFormChangesWarning,
		preventMultipleFormSubmission: preventMultipleFormSubmission,
	};
})();

// CSS
import "./styles/admin.scss";
