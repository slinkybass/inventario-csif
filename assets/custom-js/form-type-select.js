import Autocomplete from "./autocomplete.js";

document.addEventListener("DOMContentLoaded", () => {
	const autocomplete = new Autocomplete();
	document.querySelectorAll('[data-ea-widget="ea-autocomplete"]').forEach((element) => {
		autocomplete.create(element);
	});
});
