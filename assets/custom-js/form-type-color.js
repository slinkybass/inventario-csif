import "spectrum-colorpicker2/dist/spectrum.js";
import "spectrum-colorpicker2/dist/spectrum.css";

document.addEventListener("DOMContentLoaded", () => {
	document.querySelectorAll("[data-ea-color-field]").forEach((element) => {
		$(element).spectrum({
			type: "component",
			showPalette: false,
			showButtons: false,
			showAlpha: false,
		});
	});
});
