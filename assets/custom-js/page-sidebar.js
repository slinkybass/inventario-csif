class SidebarHandler {
	#sidebarCookieKey;

	constructor() {
		this.#sidebarCookieKey = 'ea/sidebar';
	}

	updateSidebar() {
		const selectedSidebar = this.#getCookie(this.#sidebarCookieKey) || 'big';
		this.#setSidebar(selectedSidebar);
	}

	createSidebarSelector() {
		if (null === document.querySelector('input[type="checkbox"][data-ea-sidebar]')) {
			return;
		}

		const switchSidebarCheckboxes = document.querySelectorAll('input[type="checkbox"][data-ea-sidebar]');
		switchSidebarCheckboxes.forEach((switchSidebarCheckbox) => {
			switchSidebarCheckbox.addEventListener('change', () => {
				const selectedSidebar = switchSidebarCheckbox.checked ? 'small' : 'big';
				this.#setSidebar(selectedSidebar);
				switchSidebarCheckboxes.forEach((otherSwitchSidebarCheckbox) => { otherSwitchSidebarCheckbox.checked = switchSidebarCheckbox.checked });
			});
		});
	}

	#setSidebar(sidebar) {
		document.body.classList.remove('ea-sidebar-small', 'ea-sidebar-big');
		document.body.classList.add('big' === sidebar ? 'ea-sidebar-big' : 'ea-sidebar-small');
		this.#setCookie(this.#sidebarCookieKey, sidebar);
	}

	#setCookie(name, value, days = 365) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days*24*60*60*1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + (value || "")  + expires + "; path=/";
	}

	#getCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}
}

const sidebarHandler = new SidebarHandler();

document.addEventListener('DOMContentLoaded', () => {
	sidebarHandler.createSidebarSelector();
});
