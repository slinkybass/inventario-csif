import flatpickr from "flatpickr";
import "flatpickr/dist/l10n/es.js";
import "flatpickr/dist/flatpickr.css";

document.addEventListener("DOMContentLoaded", () => {
	document.querySelectorAll("[data-ea-date-field]").forEach((element) => {
		const date = flatpickr(element, {
			locale: moment.locale(),
			minuteIncrement: 1,
			altInput: true,
			allowInput: true,
			dateFormat: "YYYY-MM-DD",
			altFormat: moment.localeData().longDateFormat("L"),
			formatDate: (date, format, locale) => {
				return moment(date).format(format);
			},
			parseDate: (datestr, format) => {
				return moment(datestr, format, true).toDate();
			},
		});
	});
});
