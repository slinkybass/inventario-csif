Required:
PHP 8, composer, yarn, mysql

Install:
composer install
yarn
yarn encore dev

Create Database if don't exists:
php bin/console doctrine:database:create

Update Database:
php bin/console make:migration && php bin/console doctrine:migrations:migrate

Create Users:
php bin/console app:create-users

Update permissions for ROLE_SUPERADMIN:
php bin/console app:update-permissions

USEFUL COMMANDS:
Create Entity:
php bin/console make:entity

Create Controller:
php bin/console make:admin:crud
(place in src/Controller/Admin/Cruds/)